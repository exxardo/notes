public class OrdemDeCompra {
    
    private List<Produto> produtos = new ArrayList<>();
    
    public void adicionalProduto(Produto produto) { produtos.add(produto); }
    
    public void removerProduto(Produto produto) { produtos.remove(produto); }
    
    public BigDecimal calcularTotal() {
        return produtos.stream()
            .map(Produto::getValor)
            .reduce(BigDecimal.ZERO, BigDecimal::add);
    }