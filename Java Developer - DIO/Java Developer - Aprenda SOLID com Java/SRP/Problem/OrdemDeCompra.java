package SRP.problem;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

public class OrdemDeCompra {
    
    private List<Produto> produtos = new ArrayList<>();
    
    public void adicionalProduto(Produto produto) { produtos.add(produto); }
    
    public void removerProduto(Produto produto) { produtos.remove(produto); }
    
    public BigDecimal calcularTotal() {
        return produtos.stream()
            .map(Produto::getValor)
            .reduce(BigDecimal.ZERO, BigDecimal::add);
    }
    
    public List<OrdemDeCompra> buscaOrdemDeCompra() {
        //retornar a lista de ordem de compra da base de dados
        return new ArrayList<>();
    }
    
    public void salvarOrdemCompra() {
        //salvar lista de produtos na base de dados
    }
    
    public void enviaEmail(String email) {
        //enviar email da ordem de compra
    }
    
    public void imprimiOrdemDeCompra() {
        //imprimir a ordem de compra
    }
}