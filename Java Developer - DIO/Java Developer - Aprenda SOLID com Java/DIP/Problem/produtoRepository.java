package DIP.problem;

import java.util.ArrayList;
import java.util.List;

public class ProdutoRepository {
    
    private MySqlConnection mySqlConnection;
    
    public ProdutoRepository() {
        this.mySqlConnection = new MySqlConnection();
    }
    
    public List<Produt> buscaProdutos() {
        //retorna a lista de produtos da base de dados
        return ArrayList<>();
    }
    
    public void salvarProduto(Produto produto) {
        //salva lista de produtos na base de dados
    }
}