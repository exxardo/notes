# Aprenda sobre S.O.L.I.D. com Java

## Java Developer - DIO

------

### Aula 2

*Single Responsibility Principle*

**Uma classe deve ter um**, e somente um, **motivo para mudar**. A classe deve possuir uma única responsabilidade dentro do software.

São as famosas "God class", que fazem muitas coisas e se tornam difíceis de serem lidas, refatoradas.

###### Exemplo de classe com muita responsabilidade:

```java
package SRP.problem;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

public class OrdemDeCompra {
    
    private List<Produto> produtos = new ArrayList<>();
    
    public void adicionalProduto(Produto produto) { produtos.add(produto); }
    
    public void removerProduto(Produto produto) { produtos.remove(produto); }
    
    public BigDecimal calcularTotal() {
        return produtos.stream()
            .map(Produto::getValor)
            .reduce(BigDecimal.ZERO, BigDecimal::add);
    }
    
    public List<OrdemDeCompra> buscaOrdemDeCompra() {
        //retornar a lista de ordem de compra da base de dados
        return new ArrayList<>();
    }
    
    public void salvarOrdemCompra() {
        //salvar lista de produtos na base de dados
    }
    
    public void enviaEmail(String email) {
        //enviar email da ordem de compra
    }
    
    public void imprimiOrdemDeCompra() {
        //imprimir a ordem de compra
    }
}
```

Para evitar que isso ocorra devemos segregar em classes separadas para que não fique com diversos métodos e processos:

###### CLASSE `OrdemDeCompra`

```java
public class OrdemDeCompra {
    
    private List<Produto> produtos = new ArrayList<>();
    
    public void adicionalProduto(Produto produto) { produtos.add(produto); }
    
    public void removerProduto(Produto produto) { produtos.remove(produto); }
    
    public BigDecimal calcularTotal() {
        return produtos.stream()
            .map(Produto::getValor)
            .reduce(BigDecimal.ZERO, BigDecimal::add);
    }
```

###### CLASSE `OrdemDeCompraRepository`

```java
class OrdemDeCompraRepository {
    public List<OrdemDeCompra> buscaOrdemDeCompra() {
        //retornar a lista de ordem de compra da base de dados
        return new ArrayList<>();
    }
    
    public void salvarOrdemCompra() {
        //salvar lista de produtos na base de dados
    }
}
```

###### CLASSE `OrdemDeCompraMail`

```java
class OrdemDeCompraMail {
    public void enviaEmail(String email) {
        //enviar email da ordem de compra
    }
}
```

###### CLASS `OrdemDeCompraPrint` 

```java
class OrdemDeCompraPrint {
	public void imprimiOrdemDeCompra() {
        //imprimir a ordem de compra
    }
}
```



------

### Aula 3

*Open Closed Principle*

> Você deve poder estender um comportamento de classe, sem modificá-lo.

Objetos devem estar **abertos para extensão**, mas **fechados para modificações**. Quando novos comportamentos precisam ser adicionados no software, devemos estender e não alterar o código fonte original.

```java
package OCP.problem;

public class ControladorDeDesconto {
    
    public void adicionaDescontoLivroInfantil(DescontoLivroInfantil descontoLivroInfantil) {
        descontoLivroInfantil.valorDescontoLivroInfantil();
    }
    
    public void adicionaDescontoLivroAutoAjuda(DescontoLivroAutoAjuda descontoLivroAutoAjuda) {
        descontoLivroAutoAjuda.valorDescontoLivroAutoAjuda();
    }
}
```

```java
package OCP.problem;

public class DescontoLivroInfantil {
    
    public double valorDescontoLivroInfantil(){
        return 0.3;
    }
}
```

```java
package OCP.problem;

public class DescontoLivroAutoAjuda {
    
    public double valorDescontoLivroAutoAjuda(){
        return 0.1;
    }
}
```

Dessa forma teríamos que estar crescendo os métodos sempre que fossemos adicionar novos descontos e assim quebrando o principio. Podemos resolver criando uma `interface`.

```java
package OCP.solution;

public class ControladorDeDesconto {
    
    public void adicionaDescontoLivro(DescontoLivro descontoLivro) {
        descontoLivro.valorDesconto();
    }
}
```

```java
package OCP.solution;

public interface DescontoLivro {
    
    double valorDesconto();
}
```

###### Adicionando novos descontos:

```java
package OCP.solution;

public class DescontoLivroInfantil implements DescontoLivro {
    @Override
    public double valorDesconto() {
        return 0.2;
    }
}
```

E assim não precisamos alterar nada no `ControladorDeDesconto`, mantendo assim o principio. Estendemos mas não modificamos.

------

### Aula 4

*Liskov Substitution Principle*

> Classes derivadas devem ser substituíveis por suas classes base.

O princípio da substituição de Liskov foi introduzido por Barbara Liskov em 1987:

> Se para cada objeto **o1** do **tipo S** há um objeto **o2** do **tipo T** de forma que, para todos os **programas P**, o comportamento de **P** é inalterado quando **o1** é substituído por **o2**, então **S é um subtipo de T**.

```java
package LSP.problem;

public class Quadrado extends Retangulo {
	@Override
	public void setAltura(double altura) {
		super.setAltura(altura);
		super.setLargura(altura);
	}
	
	@Override
	public void setLargura(double largura) {
		super.setLargura(largura);
		super.setAltura(largura);
	}
}
```

```java
package LSP.problem;

public class Main {
    public static void main(String[] args) {
        Retangulo retangulo = new Quadrado();
        
        retangulo.setAltura(10);
        retangulo.setLargura(5);
        
        System.out.printIn("Area: " + retangulo.getArea())
    }
}
```

```java
package LSP.problem;

public class Retangulo {
    private double altura;
    private double largura;
    
    public double getAltura() {
        return altura;
    }
    
    public void setAltura(double altura) {
        this.altura = altura
    }
    
    public double getLargura() {
        return largura;
    }
    
    public void setLargura(double largura) {
        this.largura = largura
    }
    
    public double getArea() { return altura * largura; }
}
```

```java
Area: 25.0
```

Retornou  25, mas não deveria ser 50? Já que é a ALTURAxLARGURA (10x5). O retângulo, na verdade, é um quadrado.

Quando tu dá um "set" tanto na largura quando altura, ele modifica os dois atributos. Ou seja, quando foi feito o `setLargura` com 5, ele modificou o `setAltura` também.

Para ficar um código de acordo com o S.O.L.I.D., cada um deveria calcular sua própria área. Nem toda abstração funciona sem que ela seja feita com sua classe base.

------

### Aula 5

*Interface Segregation Principle*

> Faça interfaces refinadas que são específicas do cliente.

Uma classe **não deve** ser forçada a **implementar** interfaces e **métodos** que **não serão utilizados**. É melhor criar **interfaces** mais **específicas** ao **invés de** termos uma única **interface genérica**.

```java
package ISP.problem;

public interface Ave {
    
    void bicar();
    
    void chocarOvos();
    
    void voar();
}
```

```java
package ISP.problem;

public class Pato implements Ave{
    
    @Override
    public void bicar() {
        //consegue bicar
    }
    
    @Override
    public void chocarOvos(){
        //consegue chocar os ovos
    }
    
    @Override
    public void voar() {
        //consegue voar
    }
}
```

```java
package ISP.problem;

public class Pinguim implements Ave {
    @Override
    public void bicar() {
        //consegue bicar
    }
    
    @Override
    public void chocarOvos() {
        //consegue chocar os ovos
    }
    
    @Override
    public void voar() {
        //NÃO CONSEGUE VOAR
    }
}
```

Ocorreu uma violação do principio, já que nem todas  as aves conseguem voar, então a interface já não atende a todas as aves. Devemos então segregar.

###### **Identificação global de ave:**

```java
package ISP.solution;

public interface Ave {
    void bicar();
    
    void chocarOvos();
}
```

###### **Interface para aves que voam**

```java
package ISP.solution;

public interface AvesVoam extends Ave {
    void voar();
}
```

###### O `Pato` não deixa de ser uma ave que pode voar

```java
package ISP.solution;

public class Pato implements AvesVoam {
    
    @Override
    public void bicar() {
        //consegue bicar
    }
    
    @Override
    public void chocarOvos(){
        //consegue chocar os ovos
    }
    
    @Override
    public void voar() {
        //consegue voar
    }
}
```

###### O `Pinguim` não deixa de implementar os atributos de uma ave, mas com a diferença que não voa.

```java
package ISP.solution;

public class Pinguim implements Ave {
    @Override
    public void bicar() {
        //consegue bicar
    }
    
    @Override
    public void chocarOvos() {
        //consegue chocar os ovos
    }
}
```

------

### Aula 6

*Dependency inversion Principle*

> Dependa de abstrações e não de implementações.

Um módulo de alto nível não deve depender de módulos de baixo nível, ambos devem depender da abstração.

- Inversão de Dependência **não é igual** a Injeção de Dependência.

Na prática sempre devemos depender de interfaces, não de classes concretas.

```java
package DIP.problem;

import java.util.ArrayList;
import java.util.List;

public class ProdutoRepository {
    
    private MySqlConnection mySqlConnection;
    
    public ProdutoRepository() {
        this.mySqlConnection = new MySqlConnection();
    }
    
    public List<Produt> buscaProdutos() {
        //retorna a lista de produtos da base de dados
        return ArrayList<>();
    }
    
    public void salvarProduto(Produto produto) {
        //salva lista de produtos na base de dados
    }
}
```

###### Como solução podemos criar uma `interface` e assim podemos utilizar o `ProdutoRepository` independentemente do banco de dados utilizado, já que não precisamos mexer no `DbConnection`:

```java
package DIP.solution;

public interface DbConnection {
	//métodos de conexão
}
```

```java
package DIP.solution;

import java.util.ArrayList;
import java.util.List;

public class ProdutoRepository {
    
    private DbConnection dbConnection;
    
    public ProdutoRepository(DbConnection dbConnection) {
        this.dbConnection = dbConnection;
    }
    
    public List<DIP.problem.Produto> buscaProdutos() {
        //retorna a lista de produtos da base de dados
        return new ArrayList<>();
    }
    
    public void salvarProduto(Produto produto) {
        //salva lista de produtos na base de dados
    }
}
```

```java
package DIP.solution;

public class OracleConnection implements DbConnection {
}
```

```java
package DIP.solution;

public class MySqlConnection implements DbConnection {
}
```

