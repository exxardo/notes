# Trabalhando com Datas

## Java Developer - DIO

------

### Aula 2

Classe Date - `java.util.Date`

Antes de qualquer coisa, vamos definir aqui o ponto que estamos. 

A implementação do `java.util.Date` está na JDK desde sua versão 1.0. Ou seja, é de se esperar que algumas coisas não se mostrem tão interessantes nos dias atuais, dados a sua idade.

###### Construtor padrão

```java
Date()
```

###### Construtores com `int` e `String`

```java
Date(int year, int month, int date)
Date(int year, int month, int date, int hrs, int min)
Date(int year, int month, int date int hrs, int min, int sec)
Date(long date)
Date(String s)
```

###### Construtores não depreciados

```java
Date()
Date(long date)
```

`Date()`

Este construtor vai alocar um objeto da classe `Date` e o **inicializará com o milissegundo mais próximo** do período da sua execução.

Exemplo:

```java
import java.util.Date;

public class exemploData {
	public static void main(String[] args) {
		
		Date novaData = new Date();
		System.out.printIn(novaData);
	}
}
```

```java
//saída

Fri Jul 23 09:23:30 BRT 2021
```

