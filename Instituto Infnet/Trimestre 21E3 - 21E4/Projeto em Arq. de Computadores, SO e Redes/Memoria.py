import psutil

def memoria():
    mem = psutil.virtual_memory()
    capacidade = round(mem.total / (1024 * 1024 * 1024), 1)
    print(f'Memória total: {capacidade} GB')
    
memoria()