import cpuinfo

info = cpuinfo.get_cpu_info()

for item in info:
    print(f'{item}: {info[item]}')
    
print(info['arch_string_raw'])