import psutil

disco = psutil.disk_usage('.')
dis_total = round(disco.total / (1024 * 1024 * 1024), 1)
dis_usado = round(disco.used / (1024 * 1024 * 1024), 1)
dis_livre = round(disco.free / (1024 * 1024 * 1024), 1)
dis_perc = disco.percent

print(f'Total: {dis_total} GB')
print(f'Em uso: {dis_usado} GB')
print(f'Livre: {dis_livre} GB')

print(f'Percentual de Disco Usado: {dis_perc}%')
