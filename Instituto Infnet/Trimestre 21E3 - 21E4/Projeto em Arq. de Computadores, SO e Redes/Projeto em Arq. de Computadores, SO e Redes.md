# Projeto em Arquitetura de Computadores, S.O. e Redes

Materiais da aula

----

### Sexta-feira, 23/07/2021

Estrutura do bloco:

- Arquitetura de Computadores e Sistemas Operacionais;
- Fundamentos do Desenvolvimento Python;
- Arquitetura de Computadores e Sistemas Operacionais;
- Fundamentos do Desenvolvimento Python.

-----------

### Sexta-feira, 30/07/2021

Objetivos da etapa:

- Desenvolver e capturar informações gerais e básicas do computador usando Python.

#### Memória Principal

Uma função do módulo psutil que pode ser usada para capturar informações básicas da memória principal é: `psutil.virtual_memory()`

Ela retorna informações numéricas sobre o uso de memória principal do sistema. O valor de retorno é uma tupla cujos componentes podemos acessar pelo nome. Os nomes são relacionados às medidas. Os principais são:

- **total**: total de memória principal em bytes.
- **available**: memória disponível para alocação de dados e processos (também em bytes).

Para obter a memória total em bytes, o comando é:

```python
import psutil

def memoria():
    mem = psutil.virtual_memory()
    print(mem.total)
    
memoria()
```

Caso queira converter para GB, divida o valor por 1024x1024x1024.

![Tabela de conversão](https://github.com/exxardo/assets/blob/main/Tabela%20convers%C3%A3o.png)

```python
import psutil

def memoria():
    mem = psutil.virtual_memory()
    capacidade = round(mem.total / (1024 * 1024 * 1024), 1)
    print(f'Memória total: {capacidade} GB')
    
memoria()
```

Você pode também arredondar a parte decimal para 2 dígitos usando o comando round.

A saída para código acima será: `Capacidade total de MP: 3.77 GB`

[psutil documentation](https://psutil.readthedocs.io/en/latest/)

#### Processador ( `platform.processor()`)

Informações da arquitetura do processador encontram-se em um módulo chamado plataform. Ele é um módulo interno ao interpretador de Python e não precisa ser instalado. Para usá-lo faça: `import platform`

Com ele, é possível obter características do processador, como o nome e modelo. Além disso, estão disponíveis também informações sobre o sistema operacional.

Comando `platform.processor()` retorna o nome do processador associado à quantidade de bits que ele trabalha (32 bits ou 64 bits, normalmente).

O comando `platform.node()` indica o nome amigável do computador na rede.

O comando `platform.platform()` informa algo mais detalhado sobre a plataforma.

O comando `platform.system()` indica o nome do sistema operacional.

```python
import platform

print(platform.processor())
print(platform.node())
print(platform.platform())
print(platform.system())
```

[platform - Access to underlying platform’s identifying data](https://docs.python.org/3/library/platform.html)

O módulo psutil fornece informações relacionadas ao processamento do processador. É possível obter, por exemplo, o uso de processamento através do comando:

```python
import psutil

print(psutil.cpu_percent())
```

Este comando obtém o percentual corrente de uso de CPU (processador) no intervalo de tempo indicado. Se nada for indicado, este intervalo é None ou 0. Com este intervalo, este comando calcula o tempo de acordo com a última medida obtida. Uma maneira interessante de usá-lo é chamando o comando de tempos em tempos.

```python
import psutil
import time

for i in range(0, 100):
    print(psutil.cpu_percent())
    time.sleep(5)
```

O programa acima irá produzir informação de uso de processamento a cada segundo, 100 vezes. O comando `time.sleep(1)` serve para esperar 1 segundo até a próxima leitura.

#### Capturando informações básicas de Disco

O módulo psutil também oferece maneiras de se obter o uso de disco do computador. É comum o computador ter mais de um disco (fixo ou removível) além da possibilidade de particionamento.

O comando de nome `psutil.disk_usage` indica o uso de um disco com o caminho de localização passado como parâmetro. Usando '.', o caminho é o corrente. No entanto, você pode ver outras partições que também que foram criadas no seu computador e que estejam disponíveis.

```python
import psutil

disco = psutil.disk_usage('.')

print("Total:", disco.total, "B")
print("Em uso:", disco.used, "B")
print("Livre:", disco.free, "B")

print("Total:", round(disco.total/(1024*1024*1024), 2), "GB")
print("Em uso:", round(disco.used/(1024*1024*1024), 2), "GB")
print("Livre:", round(disco.free/(1024*1024*1024), 2), "GB")

print("Percentual de Disco Usado:", disco.percent)
```

O comando `psutil.disk_usage` é chamado na linha 3 e ele retorna uma tupla contendo informações sobre a capacidade total (em bytes), capacidade em uso (em bytes) capacidade livre (em bytes) e o percentual de uso.

```python
disco = psutil.disk_usage('.')
```

As linhas 5, 6 e 7 mostram um print da capacidade total, em uso e livre em bytes.

```python
print("Total:", disco.total, "B")
print("Em uso:", disco.used, "B")
print("Livre:", disco.free, "B")
```

As linhas 9, 10 e 11 mostram um print da capacidade total, em uso e livre em GB. A função round é usada para arredondar o valor gerado em GB para apenas duas casas decimais.

```python
print("Total:", round(disco.total/(1024*1024*1024), 2), "GB")
print("Em uso:", round(disco.used/(1024*1024*1024), 2), "GB")
print("Livre:", round(disco.free/(1024*1024*1024), 2), "GB")
```

A última linha, 13, imprime o percentual de uso do disco.

```python
print("Percentual de Disco Usado:", disco.percent)
```

-------

### Sexta-feira, 06/08/2021

#### Capturando informações básicas de Rede

Uma informação básica de rede é capturar o próprio IP da máquina em que o seu programa é executado. No entanto, não é uma tarefa muito simples de entender.

O psutil apresenta um comando que retorna um dicionário para todas as possíveis interfaces de rede. Por exemplo, uma para a interface de rede sem fio (normalmente chamada de wlan#), outra para a interface de rede cabeada Ethernet (normalmente chamada de eth#). O valor de # é um número associado a interface. Quando se tem apenas uma interface de rede de cada, normalmente o seu valor é 0.

O comando é: `psutil.net_if_addrs()`

Para acessar uma interface neste dicionário, basta passar o nome dela. Para isso, você precisa saber qual é o nome da interface de rede que te interessa. Veja o exemplo abaixo que obtém informações da interface de rede sem fio wlan0.

[Network](https://psutil.readthedocs.io/en/latest/#network)

```python
import psutil

dic_interfaces = psutil.net_if_addrs()
print(dic_interfaces['Conexão Local* 1'][0].address)
```

OBS: Para ver qual a conexão, deve-se utilizar no shell o comando `dic_interfaces`. Tudo entre aspas simples e terminado com dois pontos é uma rede.

#### Criando uma Interface Visual Simples com PyGame

A partir de agora vamos criar uma interface gráfica simples para mostrar os componentes vistos anteriormente. São eles:

- uso de memória,
- uso de CPU,
- uso de disco e
- IP do computador.

Exceto para o IP do computador, todos eles serão apresentados como uma barra indicativa de quanto está em uso e quanto está livre. O IP será apresentado apenas como texto.

##### Exibindo Barra de Uso de Memória

O objetivo desta seção é mostrar como exibir o uso de memória usando elementos gráficos. A figura seguinte mostra um exemplo simples do que pode ser feito:

![Uso memoria](https://github.com/exxardo/assets/blob/main/exemplo%20uso%20memoria.png)

Abertura da janela

```python
import pygame

# Iniciando a janela principal
largura_tela = 800 # Pixels
altura_tela = 600 # Pixels
tela = pygame.display.set_mode((largura_tela, altura_tela))
pygame.display.set_caption("Uso de memória")
pygame.display.init()

# Cria relógio
clock = pygame.time.Clock()

terminou = False
while not terminou:
    # Checar os eventos do mouse aqui:
    for event in pygame.event.get():
        if event.type == pygame.QUIT:
            terminou = True
    # Atualiza o desenho na tela
    pygame.display.update()
    # 60 frames por segundo
    clock.tick(60)

# Finaliza a janela
pygame.display.quit()
```

Nenhuma informação será exibida apenas uma tela preta. A ideia deste programa é, primeiro, criar uma janela com as dimensões 800x600, configurando o título dela para “Uso de memória” na linha 7.

Na linha 11 um relógio é criado para lidar com o tempo dentro do bloco do while. O comando de repetição while é usado para ficar todo o tempo testando para checar se o botão de sair da janela foi clicado. Além disso, ele será usado para desenhar a barra de status de memória, que deve ser atualizada de tempos em tempos para passar a condição atual do uso de memória.

```python
clock = pygame.time.Clock()

terminou = False
while not terminou:
    # Checar os eventos do mouse aqui:
    for event in pygame.event.get():
		if event.type == pygame.QUIT:
			terminou = True
			# Atualiza o desenho na tela
             pygame.display.update()
             # 60 frames por segundo
             clock.tick(60)
```

Antes das linhas 19 e 20, você deve colocar seu código para desenhar os retângulos que irão representar as barras, uma azul e outra vermelha. Para simplificar e organizar, o código cria uma função para isso no início do programa:

Código 2:

```python
#...
pygame.font.init()
font = pygame.font.Font(None, 32)

# Mostar uso de memória
def mostra_uso_memoria():
	mem = psutil.virtual_memory()
    larg = largura_tela - 2*20
    tela.fill(preto)
    pygame.draw.rect(tela, azul, (20, 50, larg, 70))
    larg = larg*mem.percent/100
    pygame.draw.rect(tela, vermelho, (20, 50, larg, 70))
    total = round(mem.total/(1024*1024*1024),2)
    texto_barra = "Uso de Memória (Total: " + str(total) + "GB):"
    text = font.render(texto_barra, 1, branco)
    tela.blit(text, (20, 10))
```

O Código 2 mostra como pode ser escrita uma função para mostrar o uso de memória. **Não esqueça de incluir o** `import psutil` **no início de seu programa.**

As linhas 2 e 3 não fazem parte da função, mas são importantes para escrever o texto que fica acima da barra.

```python
pygame.font.init()
font = pygame.font.Font(None, 32)
```

A partir da linha 6, temos a definição da função mostra_uso_memoria. Esta função começa obtendo informações de memória na linha 7. A linha 8 calcula a largura da barra como sendo a largura da tela menos 2*20, onde 20 é o valor da margem lateral.

```
  def mostra_uso_memoria():
  mem = psutil.virtual_memory()
  larg = largura_tela - 2*20
  
```

Na linha 9, a tela é apagada, isto é, preenchida de preto. Na linha 10, um retângulo com a largura indicada é desenhado. Ele fica na posição 20x50 da tela, tem largura definida anteriormente e altura fixa de 70 unidades e sua cor é azul.

**Não esqueça de definir a cor azul antes como sendo (0, 0, 255).**

```
  tela.fill(preto)
  pygame.draw.rect(tela, azul, (20, 50, larg, 70))
  
```

Já a segunda barra tem a sua largura alterada de acordo com o valor do percentual de memória. A largura é alterada na linha 11 e, na linha 12, o retângulo é desenhado na mesma posição e altura que o anterior, mas com a largura relativa à mem.percent (o percentual de uso de memória obtido naquele momento). Além disso, tem a cor vermelha.

```
  larg = larg*mem.percent/100
  pygame.draw.rect(tela, vermelho, (20, 50, larg, 70))
  
```

As linhas de 13 a 16 servem para desenhar o texto na cor branca na tela, acima dos retângulos.

**Não esqueça de definir a cor vermelha antes como sendo (255, 0, 0) e a cor branca como sendo (255, 255, 255).**

```
  total = round(mem.total/(1024*1024*1024),2)
  texto_barra = "Uso de Memória (Total: " + str(total) + "GB):"
  text = font.render(texto_barra, 1, branco)
  tela.blit(text, (20, 10))
  
```

Com a função pronta, basta agora chamá-la dentro do while, antes de atualizar a tela. Ainda, para fazer com que a alteração ocorra em uma frequência menor, adicione um contador para indicar depois de quantas vezes deve-se realizar a atualização das barras. Veja um trecho de código de como ficaria isso:

Código 3:

```python
#...
# Contador de tempo
cont = 60

terminou = False

while not terminou:
	# Checar os eventos do mouse aqui:
	for event in pygame.event.get():
		if event.type == pygame.QUIT:
			terminou = True
             # Fazer a atualização a cada segundo:
     if cont == 60:
		mostra_uso_memoria()
		cont = 0
	# Atualiza o desenho na tela
	pygame.display.update()
	# 60 frames por segundo
	clock.tick(60)
	cont = cont + 1
```

Código até o momento:

```python
import pygame
import psutil

# Definindo cores
azul = (106, 90, 205)
vermelho = (255, 99, 71)
branco = (255, 255, 255)
preto = (0, 0, 0)

pygame.font.init()
font = pygame.font.Font(None, 32)

def mostra_uso_memoria():
    mem = psutil.virtual_memory()
    larg = largura_tela - 2*20
    tela.fill(preto)
    pygame.draw.rect(tela, azul, (20, 50, larg, 70))
    larg = larg*mem.percent / 100
    pygame.draw.rect(tela, vermelho, (20, 50, larg, 70))
    total = round(mem.total/(1024 * 1024 * 1024), 2)
    texto_barra = f'Uso de Memória (Total: {total} GB):'
    text = font.render(texto_barra, 1, branco)
    tela.blit(text, (20, 10))

# Iniciando a janela principal
largura_tela = 600 # Pixels
altura_tela = 160 # Pixels
tela = pygame.display.set_mode((largura_tela, altura_tela))
pygame.display.set_caption('Uso de memória')
pygame.display.init()

# Cria relógio
clock = pygame.time.Clock()

cont = 60

terminou = False

while not terminou:
    # Checar os eventos do mouse aqui:
    for event in pygame.event.get():
        if event.type == pygame.QUIT:
            terminou = True
    if cont == 60:
        mostra_uso_memoria()
        cont = 0
        
    # Atualiza o desenho na tela
    pygame.display.update()
    # 60 frames por segundo
    clock.tick(60)
    cont += 1

# Finaliza a janela
pygame.display.quit()
```

##### Exibindo Barra de Uso de CPU

![Uso CPU](https://github.com/exxardo/assets/blob/main/exemplo%20uso%20cpu.png)

Uma vez tendo feito a de memória, basta escrever uma função para a CPU também.

```python
 pygame.font.init()
  font = pygame.font.Font(None, 32)

  # Mostrar uso de CPU:
  def mostra_uso_cpu():
      capacidade = psutil.cpu_percent(interval=0)
      larg = largura_tela - 2*20
      tela.fill(preto)
      pygame.draw.rect(tela, azul, (20, 50, larg, 70))
      larg = larg*capacidade/100
      pygame.draw.rect(tela, vermelho, (20, 50, larg, 70))
      text = font.render("Uso de CPU:", 1, branco)
      tela.blit(text, (20, 10))
```

Teremos o código assim:

```python
import pygame
import psutil

# Definindo cores
azul = (106, 90, 205)
vermelho = (255, 99, 71)
branco = (255, 255, 255)
preto = (0, 0, 0)

pygame.font.init()
font = pygame.font.Font(None, 32)

def mostra_uso_memoria():
    mem = psutil.virtual_memory()
    larg = largura_tela - 2*20
    tela.fill(preto)
    pygame.draw.rect(tela, azul, (20, 50, larg, 70))
    larg = larg*mem.percent / 100
    pygame.draw.rect(tela, vermelho, (20, 50, larg, 70))
    total = round(mem.total/(1024 * 1024 * 1024), 2)
    texto_barra = f'Uso de Memória (Total: {total} GB):'
    text = font.render(texto_barra, 1, branco)
    tela.blit(text, (20, 10))

pygame.font.init()
font = pygame.font.Font(None, 32)

# Mostrar uso de CPU:
def mostra_uso_cpu():
    capacidade = psutil.cpu_percent(interval=0)
    larg = largura_tela - 2*20
    tela.fill(preto)
    pygame.draw.rect(tela, azul, (20, 50, larg, 70))
    larg = larg*capacidade/100
    pygame.draw.rect(tela, vermelho, (20, 50, larg, 70))
    text = font.render(f'Uso de CPU: {capacidade}', 1, branco)
    tela.blit(text, (20, 10))

# Iniciando a janela principal
largura_tela = 600 # Pixels
altura_tela = 160 # Pixels
tela = pygame.display.set_mode((largura_tela, altura_tela))
pygame.display.set_caption('Uso de memória')
pygame.display.init()

# Cria relógio
clock = pygame.time.Clock()

cont = 60

terminou = False

while not terminou:
    # Checar os eventos do mouse aqui:
    for event in pygame.event.get():
        if event.type == pygame.QUIT:
            terminou = True
    if cont == 60:
        mostra_uso_memoria()
        mostra_uso_cpu()
        cont = 0
        
    # Atualiza o desenho na tela
    pygame.display.update()
    # 60 frames por segundo
    clock.tick(60)
    cont += 1

# Finaliza a janela
pygame.display.quit()
```

##### Exibindo Barra de Uso de Disco

![Uso disco](https://github.com/exxardo/assets/blob/main/exemplo%20uso%20disco.png)

```python
pygame.font.init()
font = pygame.font.Font(None, 32)

# Mostrar o uso de disco local
def mostra_uso_disco():
    disco = psutil.disk_usage('.')
    larg = largura_tela - 2*20
    tela.fill(preto)
    pygame.draw.rect(tela, azul, (20, 50, larg, 70))
    larg = larg*disco.percent/100
    pygame.draw.rect(tela, vermelho, (20, 50, larg, 70))
    total = round(disco.total/(1024*1024*1024), 2)
    texto_barra = "Uso de Disco: (Total: " + str(total) + "GB):"
    text = font.render(texto_barra, 1, branco)
    tela.blit(text, (20, 10))
```

Note que o Código 5 é semelhante aos Códigos 2 e 4 e ele deve ser usado da mesma forma que ambos. As diferenças estão nas linhas 7 e 11, onde os cálculos são associados ao valor do percentual de uso do disco obtido pela função `psutil.disk_usage('.')`. O trecho, no final, que escreve o texto, também é alterado para inserir informações sobre o disco.

Teremos o código assim:

```python
import pygame
import psutil

# Definindo cores
azul = (106, 90, 205)
vermelho = (255, 99, 71)
branco = (255, 255, 255)
preto = (0, 0, 0)

pygame.font.init()
font = pygame.font.Font(None, 32)

def mostra_uso_memoria():
    mem = psutil.virtual_memory()
    larg = largura_tela - 2*20
    tela.fill(preto)
    pygame.draw.rect(tela, azul, (20, 50, larg, 70))
    larg = larg*mem.percent / 100
    pygame.draw.rect(tela, vermelho, (20, 50, larg, 70))
    total = round(mem.total/(1024 * 1024 * 1024), 2)
    texto_barra = f'Uso de Memória (Total: {total} GB):'
    text = font.render(texto_barra, 1, branco)
    tela.blit(text, (20, 10))

pygame.font.init()
font = pygame.font.Font(None, 32)

# Mostrar uso de CPU:
def mostra_uso_cpu():
    capacidade = psutil.cpu_percent(interval=0)
    larg = largura_tela - 2*20
    tela.fill(preto)
    pygame.draw.rect(tela, azul, (20, 50, larg, 70))
    larg = larg*capacidade/100
    pygame.draw.rect(tela, vermelho, (20, 50, larg, 70))
    text = font.render(f'Uso de CPU: {capacidade}', 1, branco)
    tela.blit(text, (20, 10))
    
pygame.font.init()
font = pygame.font.Font(None, 32)

# Mostrar o uso de disco local
def mostra_uso_disco():
    disco = psutil.disk_usage('.')
    larg = largura_tela - 2*20
    tela.fill(preto)
    pygame.draw.rect(tela, azul, (20, 50, larg, 70))
    larg = larg*disco.percent/100
    pygame.draw.rect(tela, vermelho, (20, 50, larg, 70))
    total = round(disco.total/(1024*1024*1024), 2)
    texto_barra = f'Uso de Disco: (Total: {total} GB)'
    text = font.render(texto_barra, 1, branco)
    tela.blit(text, (20, 10))

# Iniciando a janela principal
largura_tela = 600 # Pixels
altura_tela = 160 # Pixels
tela = pygame.display.set_mode((largura_tela, altura_tela))
pygame.display.set_caption('Uso de recursos do computador')
pygame.display.init()

# Cria relógio
clock = pygame.time.Clock()

cont = 60

terminou = False

while not terminou:
    # Checar os eventos do mouse aqui:
    for event in pygame.event.get():
        if event.type == pygame.QUIT:
            terminou = True
    if cont == 60:
        mostra_uso_memoria()
        mostra_uso_cpu()
        mostra_uso_disco()
        cont = 0
        
    # Atualiza o desenho na tela
    pygame.display.update()
    # 60 frames por segundo
    clock.tick(60)
    cont += 1

# Finaliza a janela
pygame.display.quit()
```

##### Superfícies em Pygame

Para organizar melhor os componentes em uma única janela, vamos usar uma funcionalidade do pygame chamada **surface** (superfície).

As surfaces permitem criar sub-janelas posicionadas logicamente dentro da janela principal. Basta criá-la e depois indicar onde ela vai ser posicionada dentro da tela principal. O código a seguir mostra como criar uma surface:

```python
s_mem = pygame.surface.Surface((800, 200)) 
```

A superfície `s_mem` tem dimensões 800x200.

Depois de criada, a superfície pode ser usada como se fosse uma nova janela. Isso seria útil para criar 3 superfícies e colocá-las dentro da janela uma abaixo da outra. Os desenhos dentro da superfície não sofreriam modificações em suas posições.

O código desenha 3 superfícies dentro da janela, uma abaixo da outra, cada uma com um retângulo desenhado:

```python
import pygame
import psutil

# Iniciando a janela principal
largura_tela = 800
altura_tela = 600
tela = pygame.display.set_mode((largura_tela, altura_tela))
pygame.display.set_caption("Exemplo Surface")
pygame.display.init()

azul = (0, 0, 255)

s1 = pygame.surface.Surface((largura_tela, altura_tela/3))
s2 = pygame.surface.Surface((largura_tela, altura_tela/3))
s3 = pygame.surface.Surface((largura_tela, altura_tela/3))

pygame.draw.rect(s1, azul, (20, 50, largura_tela-2*20, 70))
tela.blit(s1, (0, 0))
pygame.draw.rect(s2, azul, (20, 50, largura_tela-2*20, 70))
tela.blit(s2, (0, altura_tela/3))
pygame.draw.rect(s3, azul, (20, 50, largura_tela-2*20, 70))
tela.blit(s3, (0, 2*altura_tela/3))

# Cria relógio
clock = pygame.time.Clock()

terminou = False
while not terminou:
    # Checar os eventos do mouse aqui:
    for event in pygame.event.get():
    	if event.type == pygame.QUIT:
    	terminou = True

# Atualiza o desenho na tela
pygame.display.update()
# 60 frames por segundo
clock.tick(60)

# Finaliza a janela
pygame.display.quit()
```

Nas linhas 13, 14 e 15, as três superfícies s1, s2 e s3 são criadas, com largura igual a da tela e altura igual a altura da tela dividido por 3 (600/3 = 200).

```python
s1 = pygame.surface.Surface((largura_tela, altura_tela/3))
s2 = pygame.surface.Surface((largura_tela, altura_tela/3))
s3 = pygame.surface.Surface((largura_tela, altura_tela/3))
```

Nas linhas 18, 20 e 22, cada uma das superfícies é inserida na tela, cada uma numa posição diferente. A primeira fica na posição (0,0), a segunda na posição (0, altura_tela/3) ou (0, 200) e a terceira na posição (0, 2*altura_tela/3) ou (0, 400).

```python
tela.blit(s1, (0, 0))
pygame.draw.rect(s2, azul, (20, 50, largura_tela-2*20, 70))
tela.blit(s2, (0, altura_tela/3))
pygame.draw.rect(s3, azul, (20, 50, largura_tela-2*20, 70))
tela.blit(s3, (0, 2*altura_tela/3))
```

Em cada superfície foi desenhado um retângulo azul. Note duas coisas importantes:

I. Foi usada a superfície (e não a tela) como lugar de desenho do retângulo. Repare no primeiro parâmetro da função pygame.draw.rect nas linhas 17, 19 e 21.

```python
pygame.draw.rect(s1, azul, (20, 50, largura_tela-2*20, 70))
tela.blit(s1, (0, 0))
pygame.draw.rect(s2, azul, (20, 50, largura_tela-2*20, 70))
tela.blit(s2, (0, altura_tela/3))
pygame.draw.rect(s3, azul, (20, 50, largura_tela-2*20, 70))
tela.blit(s3, (0, 2*altura_tela/3))
```

II. As funções das linhas 17, 19 e 21 são idênticas, excetuando pela superfície de desenho. As figuras não vão ficar sobrepostas graças ao uso de superfícies, pois são elas que serão posicionadas na tela depois, nas linhas 18, 20 e 22.

```python
pygame.draw.rect(s1, azul, (20, 50, largura_tela-2*20, 70))
tela.blit(s1, (0, 0))
pygame.draw.rect(s2, azul, (20, 50, largura_tela-2*20, 70))
tela.blit(s2, (0, altura_tela/3))
pygame.draw.rect(s3, azul, (20, 50, largura_tela-2*20, 70))
tela.blit(s3, (0, 2*altura_tela/3))
```

Ao fim, teremos o código assim:

```python
import pygame
import psutil

# Definindo cores
azul = (106, 90, 205)
vermelho = (255, 99, 71)
branco = (255, 255, 255)
preto = (0, 0, 0)

pygame.font.init()
font = pygame.font.Font(None, 32)

def mostra_uso_memoria():
    mem = psutil.virtual_memory()
    larg = largura_tela - 2*20
    tela.blit(s1, (0, 0))
    pygame.draw.rect(s1, azul, (20, 50, largura_tela-2*20, 70))
    larg = larg*mem.percent / 100
    pygame.draw.rect(s1, vermelho, (20, 50, larg, 70))
    total = round(mem.total/(1024 * 1024 * 1024), 2)
    texto_barra = f'Uso de Memória (Total: {total} GB):'
    text = font.render(texto_barra, 1, branco)
    tela.blit(text, (20, 10))

pygame.font.init()
font = pygame.font.Font(None, 32)

# Mostrar uso de CPU:
def mostra_uso_cpu():
    capacidade = psutil.cpu_percent(interval=0)
    larg = largura_tela - 2*20
    tela.blit(s2, (0, altura_tela/3))
    pygame.draw.rect(s2, azul, (20, 50, largura_tela-2*20, 70))
    larg = larg*capacidade/100
    pygame.draw.rect(s2, vermelho, (20, 50, larg, 70))
    text = font.render(f'Uso de CPU: {capacidade}', 1, branco)
    tela.blit(text, (20, 210))
    
pygame.font.init()
font = pygame.font.Font(None, 32)

# Mostrar o uso de disco local
def mostra_uso_disco():
    disco = psutil.disk_usage('.')
    larg = largura_tela - 2*20
    tela.blit(s3, (0, 2*altura_tela/3))
    pygame.draw.rect(s3, azul, (20, 50, largura_tela-2*20, 70))
    larg = larg*disco.percent/100
    pygame.draw.rect(s3, vermelho, (20, 50, larg, 70))
    total = round(disco.total/(1024*1024*1024), 2)
    texto_barra = f'Uso de Disco: (Total: {total} GB)'
    text = font.render(texto_barra, 1, branco)
    tela.blit(text, (20, 410))

# Iniciando a janela principal
largura_tela = 800 # Pixels
altura_tela = 600 # Pixels
tela = pygame.display.set_mode((largura_tela, altura_tela))
pygame.display.set_caption('Uso de recursos do computador')
pygame.display.init()

s1 = pygame.surface.Surface((largura_tela, altura_tela/3))
s2 = pygame.surface.Surface((largura_tela, altura_tela/3))
s3 = pygame.surface.Surface((largura_tela, altura_tela/3))

pygame.draw.rect(s1, azul, (20, 50, largura_tela-2*20, 70))
tela.blit(s1, (0, 0))
pygame.draw.rect(s2, azul, (20, 50, largura_tela-2*20, 70))
tela.blit(s2, (0, altura_tela/3))
pygame.draw.rect(s3, azul, (20, 50, largura_tela-2*20, 70))
tela.blit(s3, (0, 2*altura_tela/3))

# Cria relógio
clock = pygame.time.Clock()

cont = 60

terminou = False

while not terminou:
    # Checar os eventos do mouse aqui:
    for event in pygame.event.get():
        if event.type == pygame.QUIT:
            terminou = True
    if cont == 60:
        mostra_uso_memoria()
        mostra_uso_cpu()
        mostra_uso_disco()
        cont = 0
        
    # Atualiza o desenho na tela
    pygame.display.update()
    # 60 frames por segundo
    clock.tick(60)
    cont += 1

# Finaliza a janela
pygame.display.quit()
```

----------

### Sexta-feira, 13/08/2021

[CPUInfo Documentação](https://pypi.org/project/py-cpuinfo/)

#### Obtendo Todas as Informações de CPU

O módulo cpu info apresenta a função `cpuinfo.get_cpu_info()`, que permite obter todas as informações do processador. Ela retorna um dicionário, que é uma estrutura de armazenamento de dados em Python que usa um par chave e valor para acessar seus valores.

Neste caso, a chave é o nome da informação. Por exemplo, para obter o tipo de arquitetura da CPU, a chave é `arch_string_raw` e o valor é `AMD64` no computador de testes. A seguir veremos o passo a passo de como obter os valores e acessar os mesmos.

```python
import cpuinfo

info = cpuinfo.get_cpu_info()

for item in info:
    print(f'{item}: {info[item]}')
    
print(info['arch_string_raw'])
```

O índice `item` varia em todas as chaves de info e o acesso ao valor é feito pela chave usando `info[item]`.

#### Obtendo Palavra

Através do módulo cpuinfo e chave ‘bits’:

```python
import cpuinfo
print(info['bits'])
```

Para o computador de testes, o resultado será:

```python
64
```

#### Número de Núcleos

Através do módulo psutil, obtém-se o número de núcleos lógicos (*cores*) da CPU:

```python
import psutil
print(psutil.cpu_count())
```

Para o computador de testes, o resultado será:

```python
12
```

Para obter o número de núcleos físicos de CPU:

```python
import psutil
print(psutil.cpu_count(logical=False))
```

Por padrão, o valor de logical é `True`. Logo, se você quiser obter o número de núcleos físicos, deve explicitamente inserir `logical=False` como parâmetro. Esse valor lógico tem a ver com o uso de Hyper Threading, uma técnica que permite simular logicamente núcleos de processamento.

#### Frequência Total

Através do módulo psutil:

```python
import psutil
print(psutil.cpu_freq())
```

Para o computador de testes, o resultado será:

```python
scpufreq(current=3193.0, min=0.0, max=3193.0)
```

Para obter apenas o valor atual de frequência de CPU (em MHz), use:

```python
psutil.cpu_freq().current
```

A frequência total também pode ser obtida pelo módulo cpuinfo.

O retorno de `psutil.cpu_freq()` é uma tupla nomeada por `scpufreq`. A tupla contém a frequência atual (current), a mínima (min) e a máxima (max). Atualmente, é comum existir processadores que variam sua frequência automaticamente de acordo com alguns critérios. Um deles, por exemplo, é o controle de energia.

### Sexta-feira, 20/08/2021

#### Obtendo Todas as Informações de CPU

O módulo cpu info apresenta a função `cpuinfo.get_cpu_info()`, que permite obter todas as informações do processador. Ela retorna um dicionário, que é uma estrutura de armazenamento de dados em Python que usa um par chave e valor para acessar seus valores.

Neste caso, a chave é o nome da informação. Por exemplo, para obter o tipo de arquitetura da CPU, a chave é *arch* e o valor é 'X86_64' no computador de testes. A seguir veremos o passo a passo de como obter os valores e acessar os mesmos.

O programa abaixo imprime todas as informações obtidas pela função:

Código 1:

```
  import cpuinfo
  info = cpuinfo.get_cpu_info()
  for i in info:
		  print(i, ":", info[i])
  
```

O índice `i` varia em todas as chaves de info e o acesso ao valor é feito pela chave usando `info[i]`.

#### Obtendo Arquitetura

Para obter um dado específico, deve-se indicar a chave. Por exemplo, para a arquitetura de CPU, a chave é ‘arch’.

```
  info['arch']
  
```

Para o computador de testes, o resultado será:

```
  X86_64
  
```

#### Obtendo Nome

Através do módulo cpuinfo e chave ‘brand’:

```
  import cpuinfo
  print(info['brand']
  
```

Para o computador de testes, o resultado será:

```
  Intel(R) Core(TM) i7-3930K CPU @ 3.20GHz
  
```

#### Obtendo Palavra

Através do módulo cpuinfo e chave ‘bits’:

```
  import cpuinfo
  print(info['bits'])
  
```

Para o computador de testes, o resultado será:

```
  64
  
```

#### Número de Núcleos

Através do módulo psutil, obtém-se o número de núcleos lógicos (*cores*) da CPU:

```
  import psutil
  print(psutil.cpu_count())
  
```

Para o computador de testes, o resultado será:

```
  12
  
```

Para obter o número de núcleos físicos de CPU:

```
  import psutil
  print(psutil.cpu_count(logical=False))
  
```

Por padrão, o valor de logical é `True`. Logo, se você quiser obter o número de núcleos físicos, deve explicitamente inserir `logical=False` como parâmetro. Esse valor lógico tem a ver com o uso de Hyper Threading, uma técnica que permite simular logicamente núcleos de processamento. Para obter mais informações sobre o assunto, acesse:

[Hyper Threading](https://pt.wikipedia.org/wiki/Hyper-threading)

#### Frequência Total

Através do módulo psutil:

```
  import psutil
  print(psutil.cpu_freq())
  
```

Para o computador de testes, o resultado será:

```
  scpufreq(current=3193.0, min=0.0, max=3193.0)
  
```

Para obter apenas o valor atual de frequência de CPU (em MHz), use:

```
  psutil.cpu_freq().current
  
```

A frequência total também pode ser obtida pelo módulo cpuinfo.

O retorno de `psutil.cpu_freq()` é uma tupla nomeada por `scpufreq`. A tupla contém a frequência atual (current), a mínima (min) e a máxima (max). Atualmente, é comum existir processadores que variam sua frequência automaticamente de acordo com alguns critérios. Um deles, por exemplo, é o controle de energia.

#### Escrevendo Informações Graficamente

Agora, vamos inserir as informações textuais não variáveis apresentadas acima utilizando o pygame. Usaremos as funções de desenho de texto.

Para facilitar o desenvolvimento, vamos criar duas funções, uma chamada mostra_info_cpu e outra chamada mostra_texto. A primeira deve mostrar as seguintes informações usando o pygame:

| Nome:              | `Intel(R) Core(TM) i7-3930K CPU @ 3.20GHz` |
| ------------------ | ------------------------------------------ |
| Arquitetura:       | `X86_64`                                   |
| Palavra (bits):    | `64`                                       |
| Frequência (MHz):  | `3193.0`                                   |
| Núcleos (físicos): | `12 (6)`                                   |

Você pode adicionar outras informações, se preferir.

Como iremos usar o pygame, temos que criar a tela principal e a repetição para capturar eventos (por enquanto, apenas o clique de sair da janela) e atualizar a tela. Vamos usar o Código 1 para isso. Note que ele não está completo, pois falta definir a função `mostra_info_cpu` na linha 42!

Código 1:

```python
  import pygame
  import psutil
  import cpuinfo

  # Obtém informações da CPU
  info_cpu = cpuinfo.get_cpu_info()

  # Cores:
  preto = (0, 0, 0)
  branco = (255, 255, 255)
  cinza = (100, 100, 100)

  # Iniciando a janela principal
  largura_tela = 800
  altura_tela = 600
  tela = pygame.display.set_mode((largura_tela, altura_tela))
  pygame.display.set_caption("Informações de CPU")
  pygame.display.init()
  # Superfície para mostrar as informações:
  s1 = pygame.surface.Surface((largura_tela, altura_tela))

  # Para usar na fonte
  pygame.font.init()
  font = pygame.font.Font(None, 24)
   	 
  # Cria relógio
  clock = pygame.time.Clock()
  # Contador de tempo
  cont = 60

  terminou = False
  # Repetição para capturar eventos e atualizar tela
  while not terminou:
	  # Checar os eventos do mouse aqui:
	  for event in pygame.event.get():
    		  if event.type == pygame.QUIT:
        		  terminou = True

	  # Fazer a atualização a cada segundo:
	  if cont == 60:
    		  mostra_info_cpu()
    		  cont = 0

	  # Atualiza o desenho na tela
	  pygame.display.update()
    
	  # 60 frames por segundo
	  clock.tick(60)
	  cont = cont + 1

  # Finaliza a janela
  pygame.display.quit()
  
```

No Código 1, linha 6, todos os dados de CPU são obtidos através da função `cpuinfo.get_cpu_info()` conforme especificado.

Na linha 21, é feito o uso de uma superfície. Lembrando que superfície no Pygame é similar a uma janela, que deverá ser adicionada à janela principal (isto será feito na função `mostra_info_cpu()`). O texto será inserido nela. Note que não é necessário seu uso. Será usado posteriormente para organizar os desenhos na tela.

Nas linhas 24 e 25, algumas funções de uso de fontes no Pygame são usadas para configurar o texto a ser exibido em `mostra_info_cpu()`.

Na linha 34, a repetição inicia. A cada tick de relógio (definido na linha 49), o programa verifica se o botão de sair foi clicado, nas linhas 36 e 37. A cada segundo, denotado por cont igual a 60 (lembrando que são 60 quadros por segundo), a função `mostra_info_cpu()`, ainda não implementada, é chamada. O código dela é:

Código 2:

```python
  # Mostra as informações de CPU escolhidas:
  def mostra_info_cpu():
	  s1.fill(branco)
	  mostra_texto(s1, "Nome:", "brand", 10)
	  mostra_texto(s1, "Arquitetura:", "arch", 30)
	  mostra_texto(s1, "Palavra (bits):", "bits", 50)
	  mostra_texto(s1, "Frequência (MHz):", "freq", 70)
	  mostra_texto(s1, "Núcleos (físicos):", "nucleos", 90)
	  tela.blit(s1, (0, 0))
  
```

E o código da função `mostra_texto` é:

Código 3:

```python
  # Mostra texto de acordo com uma chave:
  def mostra_texto(s1, nome, chave, pos_y):
	  text = font.render(nome, True, preto)
	  s1.blit(text, (10, pos_y))
	  if chave == "freq":
    		  s = str(round(psutil.cpu_freq().current, 2))
	  elif chave == "nucleos":
    		  s = str(psutil.cpu_count())
    		  s = s + " (" + str(psutil.cpu_count(logical=False)) + ")"
	  else:
    		  s = str(info_cpu[chave])
	  text = font.render(s, True, cinza)
	  s1.blit(text, (160, pos_y))
  
```

A segunda função mostra uma única linha do texto. Ela recebe como parâmetros: a superfície onde será desenhado o texto, o nome do dado da CPU (rótulo), a chave que indica do que se trata o dado, a posição na tela no eixo Y que o texto será desenhado. A chave (terceiro parâmetro) tem a ver com a chave do dicionário em info_cpu. No caso da frequência de CPU e número de núcleos, é usada outra forma de se obter o dado (do psutil). Por esta razão, as condições correspondentes são testadas nas linhas 5 e 7.

Nas linhas 3 e 4, apenas o nome é desenhado alinhado em y=10. Os dados referentes aos valores são alinhados em y=160.

Os Códigos 2 e 3 devem ser inseridos entre a linha 4 e 31 (antes do *while*) do Código 1. Normalmente coloca-se no início, antes dos *imports*. O Código 3 deve ser inserido antes do Código 2, já que ele usa a função no Código 3.

O resultado da combinação dos Códigos 1, 2 e 3, conforme especificado acima é:

#### Desenhando Barra de Uso de CPU por Núcleo

Na etapa anterior você aprendeu a desenhar uma barra para o uso de CPU em geral. No entanto, nos computadores atuais, é muito comum o processamento multinúcleos. Por isso, vamos estender o desenho da etapa anterior, referente à CPU, com as informações dos núcleos de processamento.

Para capturar a porcentagem de uso de processamento de cada núcleo do processador, usamos a mesma função utilizada na etapa anterior, mas adicionando um parâmetro a ela.

```python
  import psutil
  psutil.cpu_percent(interval=1, percpu=True)
  
```

Os parâmetros são:

- `interval`: o valor numérico indicado (maior que 0) refere-se ao tempo esperado para se calcular o percentual de uso de CPU. Sendo assim, há um tempo de espera para isso (bloqueante). Com o valor 0,0 ou None, o cálculo é feito desde a última chamada da função.

- `percpu`: se True, mostra o percentual de uso de CPU por núcleo.

A função retorna uma lista com os tempos de cada núcleo da CPU.

Para desenhar as barras, vamos criar uma função chamada `mostra_uso_cpu`. Esta função deve receber como parâmetro a lista do percentual de uso de CPU e gerar uma barra para cada núcleo. Por exemplo, se o processador tem 4 núcleos, então 4 barras devem ser desenhadas. Abaixo, um exemplo de resultado esperado.



[![img](https://lms.infnet.edu.br/moodle/pluginfile.php/911604/mod_page/content/16/UngPCMM_K6eoFasW9ntiyBvGwteKDpHjTuuxXL-ldF28NDlzbSxnZPsGhSAOEbzJOaXTjZDPU2SwJq0MPBg-tA4qFud0LcaT_MqAGulardeyBnaTPIc5vmSqbdR2vfKYqyWD3zSi.jpg)](https://lms.infnet.edu.br/moodle/pluginfile.php/911604/mod_page/content/16/UngPCMM_K6eoFasW9ntiyBvGwteKDpHjTuuxXL-ldF28NDlzbSxnZPsGhSAOEbzJOaXTjZDPU2SwJq0MPBg-tA4qFud0LcaT_MqAGulardeyBnaTPIc5vmSqbdR2vfKYqyWD3zSi.jpg)



Outro parâmetro importante para a função `mostra_uso_cpu` é a superfície. Logo, a função será definida por:

```
  def mostra_uso_cpu(s, l_cpu_percent):
  
```

Onde s é a superfície e `l_cpu_percent` é a lista contendo o percentual de uso de CPU de cada núcleo.

Para dispor as superfícies conforme a figura acima, temos que definir que a parte textual (superfície s1) deve ter tamanho largura_tela x altura_tela/5. Logo, a segunda superfície, chamada de s2, deve ter tamanho largura_tela x altura_tela*⅘ para completar a tela. Além disso, ela deve ser posicionada no ponto (0, altura_tela/5), que é onde termina a primeira superfície s1.

Podemos, então, posicionar a superfície 2, que deve ser criada antes de se chamar a função.

Código 5:

```
  def mostra_uso_cpu(s, l_cpu_percent):
	  s.fill(cinza)
	  # parte mais abaixo da tela
	  tela.blit(s, (0, altura_tela/5))
  
```

Repare que a superfície foi pintada de cinza escuro na segunda linha do código acima. Em seguida, a superfície foi posicionada na tela no ponto (`0, altura_tela/5`).

Agora, podemos inserir as barras. Por opção, nesta etapa, vamos desenhar as barras verticalmente. Para isso, precisamos entender a geometria do que desejamos fazer.

A altura de cada barra `alt` será calculada através da altura da superfície menos 2 vezes 10 (y). O valor de y corresponde a margem que será dada para desenhar as barras.

```python
  alt = s.get_height() - 2*y
  
```

A função get_height obtém a altura da superfície s.

A largura da barra `larg` será calculada através da largura da superfície menos 2 vezes a margem (x=10). Além disso, deve ser subtraído deste valor o deslocamento `desl`, que serão 7 (aqueles entre cada barra) mais 2 (um no início e outro no fim). Isto é, serão `num_cpu+1`. Isso nos dá o valor de largura de todas as barras juntas. Assim, para a largura de apenas 1 barra, temos que dividir tudo por `num_cpu`.

```
  larg = (s.get_width()-2*y - (num_cpu+1)*desl)/num_cpu
  
```

Agora, podemos percorrer a lista de percentual de uso de CPU para cada núcleo e desenhar sua barra. A posição de cada barra no eixo x deve ser deslocada de `desl` + `larg`, para indicar o espaçamento. Para isso, vamos usar uma variável d para ajudar neste deslocamento. Esta variável d será inicializada por `x` + `desl`.

```
  d = x + desl
  for i in l_cpu_percent:
    pygame.draw.rect(s, vermelho, (d, y, larg, alt))
    pygame.draw.rect(s, azul, (d, y, larg, (1-i/100)*alt))
    d = d + larg + desl
  
```

Dentro do for, primeiro, uma barra vermelha é desenhada ocupando toda a largura e altura definida para a barra.

```
  pygame.draw.rect(s, vermelho, (d, y, larg, alt))
  
```

Em seguida, é desenhada uma barra azul sobre a vermelha. Ela é calculada de acordo com o percentual de CPU em i, o elemento da lista.

```
  pygame.draw.rect(s, azul, (d, y, larg, (1-i/100)*alt))
  
```

É importante notar que ela representa o **percentual livre** de CPU (o contrário do vermelho). Logo, temos que calcular o valor complementar da porcentagem. Por exemplo, se o valor do percentual de uso de um núcleo de CPU é 20%, então o valor a ser usado para desenhar a linha azul deve ser 80%, isto é, `100% - 20%`.

O Código 5 mostra como essa função pode ser implementada:

Código 5:

```python
  def mostra_uso_cpu(s, l_cpu_percent):
	  s.fill(cinza)
	  num_cpu = len(l_cpu_percent)
	  x = y = 10
	  desl = 10
	  alt = s.get_height() - 2*y
	  larg = (s.get_width()-2*y - (num_cpu+1)*desl)/num_cpu
	  d = x + desl
	  for i in l_cpu_percent:
    	    	  pygame.draw.rect(s, vermelho, (d, y, larg, alt))
    	    	  pygame.draw.rect(s, azul, 	(d, y, larg, (1-i/100)*alt))
    	    	  d = d + larg + desl
	  # parte mais abaixo da tela e à esquerda
	  tela.blit(s, (0, altura_tela/5))
  
```

Para adicioná-la ao Código 1, chame-a após a linha 41. Você deve também chamar a função psutil.cpu_percent(percpu=True) antes de chamar essa função. Ela retorna uma lista do percentual de uso de CPU para cada núcleo e esta lista deve ser passada no segundo parâmetro da função. Além disso, deve-se também criar uma segunda superfície que o gráfico será desenhado (primeiro parâmetro da função) e isso pode ser feito na linha 22 do Código 1.

Nesta etapa, você desenvolveu um programa em Python para exibir graficamente informações mais detalhadas de monitoramento de CPU. Você aprendeu como capturar informações de arquitetura, nome, palavra (*word*), número de núcleos e frequência de operação de CPU de um computador. Você foi capaz também de apresentar tais informações graficamente através do módulo Pygame. Por último, você modificou a apresentação das barra de monitoramento de uso de CPU feitas na Etapa 4 para que seja possível mostrar barras gráficas para todos os núcleos de CPU.
