import Turtle from *

# Está recebendo um dado como parâmetro e retornando um dado final
def calcula_angulo(lados):
    angulo = 360 // lados
    return angulo

# Está apenas recebendo dados como parâmetros
def figura_geometrica(lados, angulo):
    for contador in range(lados):
        forward(50)
        left(angulo)

print('Calcule para a figura geométrica')
lados = int(input('Número de lados '))

ang = calcula_angulo(lados)
figura_geometrica(lados, ang)