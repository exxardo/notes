color_dict = {
    'red': '#FF0000',
    'green': '#008000',
    'black': '#000000',
    'white': '#FFFFFF'
}

for key in sorted(color_dict):
    print(f'A cor {key} corresponde ao código {color_dict[key]}')