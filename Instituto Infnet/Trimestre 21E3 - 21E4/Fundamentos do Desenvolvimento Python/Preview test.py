dic = {
    'Vermelho': 1,
    'Verde': 2,
    'Azul': 3
}

for key, value in dic.items():
    print(f'{key} corresponde a {dic[key]}')