lista = []
lista = ['carro', 123, 123.0, True]
print(lista)
print(lista[3])

for item in lista:
    print(item)
    
for item in range(0, len(lista)): # De 0 até o tamanho da lista
    print(f'Indice: {item}, tem o elemento {lista[item]}')
    
lista.append(123)

print(lista)

lista.remove(123)

print(lista)

lista.reverse()

print(lista)

lista.remove('carro')
lista.remove(123.0)

print(lista)

lista.sort()

print(lista)