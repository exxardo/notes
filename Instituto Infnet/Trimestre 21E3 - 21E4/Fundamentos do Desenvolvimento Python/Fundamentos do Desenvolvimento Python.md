# Fundamentos do  Desenvolvimento Python

[Materiais da aula](https://drive.google.com/drive/folders/1rmImH0vK1YGrpOQqXw8wvx1czgvJYhFA)
------

----

### **Quarta-feira, 21/07/2021**

[PEP 8 - Guia de Estilo Para Python](https://wiki.python.org.br/GuiaDeEstilo)

#### **CPython**

- CPython é a implementação principal da linguagem de programação Python, é escrita em Linguagem C.

#### **Jython**

- O Jython é uma implementação da linguagem Python que gera bytecode para máquinas Java. Com ela é possível o desenvolvimento de aplicações hibridas, já que serve como cola entre o Python e o Java.

#### **IronPython**

- Semelhante ao Jython, maas destinada ao C# e para a plataforma .NET.

#### **PyPy**

- o PyPy é uma implementação alterantiva ao CPython (que é a implementação padrão do Python). pyPy frequentemente roda mais rápido que o CPython, por usar uma JIT (compilador Just-In-Time), enquanto o Cpython é interpretado.

------

### **Segunda-feira, 26/07/2021**

#### **O computador não conhece Python!!!**

- O computador só entende binário
  - Linguagem de zeros e uns
    - 010010011101010101001010101, entendeu?
- Precisamos traduzir o código para binário.

#### **Compilação**

- Existe um software intermediário (bytecode) que vai traduzir e servir como mediador entre o código fonte e o que o computador possa entender.

![Compilador](https://github.com/exxardo/assets/blob/1443773caf9d0a814cd302098839b3e8e8b68610/image-20210809102844699.png)

- Na maioria das linguagens, antes de executar um programa, é necessário compilar o programa.
- O compilador gera um arquivo "executável"
  - Esse novo arquivo é o que será de fato executado.

#### **Python é uma linguagem interpretada**

- Não é necessário compilar o código Python.
- O interpretador Python vai lendo o código fonte, traduzindo para linguagem de máquina e executando ao mesmo tempo.

#### **Regras básicas**

- Sequência dos comando é importante
- Blocos devem ser criados usando indentação.

#### **Comentários**

- Comentários são trecos do programa voltados para a leitura por humanos, e ignorados pelo interpretador.
- Começam com o símbolo `#`
  - Tudo na linha após `#` é ignorado pelo interpretador.
- Use comentários para documentar seu código e fazer com que ele seja fácil de entender por outras pessoas.

#### **Atribuições de valores**

- Em Python, o operador de igualdade (`=`) é usado para atribuir valores às variáveis
- É equivalente ao símbolo de atribuição que usávamos no pseudocódigo
- Sempre na forma: `variável = valor ou expressão`
  - A expressão do lado direito é processada
  - O valor gerado é atribuído à variável

#### **Exemplo de programa em Python:**

```python
# Este programa calcula a área de um triângulo retângulo

altura = 15
base = 3
area = (altura * base) / 2

print(area)
```

#### **Quais são os tipos de dados disponíveis?**

- Em Python, toda variável tem um tipo
- Com isso, o computador pode saber quais operações são permitidas
- Os tipos podem ser divididos em três grupos:
  - Tipos numéricos (inteiro, float, ...)
  - Tipos textuais (caractere e string)
  - Tipo lógico (booleano)
- Os tipos são definidos dinamicamente, pelo próprio Python

```python
!pip3 install colabTurtle # Instalação no Colab
```

![Turtle 001](https://github.com/exxardo/assets/blob/d61f2483c17614210291d007daa762b5ccfbdd34/Turtle%20001.png)

```python
print("Teste Turtle")
from ColabTurtle.Turtle import *

initializeTurtle()
```

```python
forward(100) # faz caminhar 100 para a frente
left(72) # faz caminhar 72 para a esquerda
forward(100) # faz caminhar 100 para a frente
left(72) # faz caminhar 72 para a esquerda
forward(100) # faz caminhar 100 para a frente
left(72) # faz caminhar 72 para a esquerda
```

![Turtle 002](https://github.com/exxardo/assets/blob/main/Turtle%20002.png)

#### **Tipos Primitivos e Operadores**

Python precisa operar com **Tipos** e **Operadores**.

Python é tipada fortemente.

```python
a = 1.0
print(a)
print(type(a))
# Terá como saída:
# 1.0
# <class 'float'>
```

```python
a = 1
print(a)
print(type(a))
# Terá como saída:
# 1
# <class 'int'>
```

```python
a = True
print(a)
print(type(a))
# Terá como saída:
# True
# <class 'str'>
```

Operadores aritméticos: `+`, `-`, `*`, `/`, `%`, `**`, `+=`, `-=`, `*=`, `/=`, `%=`, `**=`

[Uri Online Judge (para treinamento)](https://urionlinejudge.com.br)

#### **Entrada de dados**

```python
distancia = int(input('Olá, quantos passos irei dar? '))
left(45)
forward(distancia)
```

![Turtle 003](https://github.com/exxardo/assets/blob/d99702d81467ec92d568c80b129e6bad5794bb0d/Turtle%2003.png)

É possível utilizar valores negativos e positivos para representar direções a ser tomadas

```python
distancia = int(input('Olá, quantos passos irei dar? '))
if distancia > 0:
	right(72)
	forward(distancia)
else:
	left(72)
	forward(distancia)
```

![Turtle 004](https://github.com/exxardo/assets/blob/main/Turtle%20004.png)

```python
distancia = int(input('Olá, quantos passos irei dar? '))
for passo in range(distancia + 100):
	if distancia > 0:
		right(82)
		forward(distancia + passo)
	else:
		left(82)
		forward
```

![Turtle 05](https://github.com/exxardo/assets/blob/main/Turtle%2005.png)



```python
for contador in range(4):
    forward(100)
    left(90)
```

![Turtle 006](https://github.com/exxardo/assets/blob/main/Turtle%20006.png)

```python
angulo = 0
while angulo >= 0:
	angulo = int(input('Indique um ângulo: '))
	left(angulo)
	forward(50)
```

![Turtle 007](https://github.com/exxardo/assets/blob/main/Turtle%20007.png)

#### **Funções**

Funções evitam repetições. Quando precisamos repetir algo em várias partes do código chamamos a função, afim de evitar linhas de códigos desnecessárias.

```python
def quadrado():
    for contador in range(4):
    	forward(100)
    	left(90)
```

![Turtle 008](https://github.com/exxardo/assets/blob/main/Turtle%20008.png)

-----------------

### **Quarta-feira, 28/07/2021**

[Aprenda Computação com Python 3.0](https://mange.ifrn.edu.br/python/aprenda-com-py3/index.html)

#### **Funções: passagem de parâmetros e retorno de valor**

```python
# Está recebendo um dado como parâmetro e retornando um dado final
def calcula_angulo(lados):
    angulo = 360 // lados
    return angulo

# Está apenas recebendo dados como parâmetros
def figura_geometrica(lados, angulo):
    for contador in range(lados):
        forward(50)
        left(angulo)

print('Calcule para a figura geométrica')
lados = int(input('Número de lados '))

ang = calcula_angulo(lados)
figura_geometrica(lados, ang)
```

![Turtle 009](https://github.com/exxardo/assets/blob/main/Turtle%20009.png)

**Como ver o que está instalado?**

```python
import sys

sys.modules
```

**Deixar o código mais limpo**

```python
from sys import * # O asterisco importa tudo

version
```

#### **Faça um programa que verifique se uma letra digitada é vogagitl ou consoante**

```python
letra = input('Digite um caractere: ').lower()

if letra == 'a' or letra == 'e' or letra == 'i' or letra == 'o' or letra == 'u':
    print('\nÉ uma vogal.')
else:
    print('\nÉ uma consoante.')
```

--------------------

### **Segunda-feira, 02/08/2021**

#### **Tuplas**

Tuplas são sequências de valores heterogêneos, isto quer dizer que podemos guardar qualquer tipo de valor dentro delas.

Porém, uma vez inseridos os valores de uma tupla, eles são imutáveis. Para representar uma tupla, utilizamos os parênteses.

```python
tupla = (1, 2, 3)
tupla
```

Tuplas possuem baixo consumo computacional. Quando eu preciso apenas criar e consumir dados, a tupla é uma boa. 

Se eu precismo manipular os dados, tupla não é uma boa.

```python
# Adicionando um novo valor a tupla
tupla = (1, 2, 3)
print(tupla)
tupla = tupla + (4,)
print(tupla)
```
-------
### **Quarta-feira, 04/08/2021**

#### **O jogo "Clica no Quadrado" com PyGame**

![Clica no quadrado](https://github.com/exxardo/assets/blob/main/Clica%20no%20Quadrado.png)

**Características do Jogo**

- Ele mostra o passar do tempo e o placar na parte superior e centralizada da janela;
- Cada quadradinho é colocado em uma posição aleatória na janela;
- Cada quadradinho possui uma cor aleatória também;
- Novos quadradinhos são inseridos na tela a cada segundo;
- Toda vez que um quadrado é clicado, um som de clique é disparado;
- Os quadradinhos nunca ultrapassam as dimensões da tela;
- Ao final, uma tela exibe a pontuação obtida pelo jogador até que ele feche a janela;
- O tempo já começa a contar desde o início do jogo;
- A janela pode ser fechada a qualquer momento durante o jogo.

#### **Componentes**

No jogo “Clica Quadradinho”, iremos precisar de alguns componentes para construí-lo.

**I. Quadrados**

Precisamos criar a figura básica de um **quadrado**. Para isso, utilizaremos o módulo Pygame que permite desenhar figuras básicas em 2D como retângulos, círculos, linhas, arcos, entre outras.

Além disso, faremos uso de classes. Classe é um conceito de orientação a objetos. Você irá aprender algo bem introdutório sobre o tema. Iremos utilizá-lo para facilitar a criação dos quadrados.

**II. Números aleatórios**

Python possui um módulo que fornece funções para gerar números aleatórios chamado **random**. Iremos utilizar estas funções quando formos posicionar cada quadrado na tela e quando formos atribuir uma cor a ele.

**III. Eventos do mouse**

O jogo em questão precisa reconhecer e agir sobre **eventos simples do mouse**. No caso, o evento mais importante que deve ser capturado é o **clique na tela**. Além disso, devemos saber quando um clique coincide com um quadrado desenhado na tela.

Pygame oferece funções tanto para capturar os eventos quanto para saber se o clique colidiu com a área de algum quadrado.

**IV. Listas em Python**

Uma estrutura importante de Python que estaremos sempre usando neste jogo é a **lista**. Em Python, uma lista é um conjunto de elementos. Logo, ela pode armazenar um grupo de elementos. Um exemplo de elementos no nosso jogo são os quadradinhos. Precisaremos de um conjunto deles.

Além disso, iremos inserir e remover quadradinhos deste conjunto. Vamos conhecer maneiras simples de se lidar com listas.
- **Listas em Python**

  Como já dito previamente, uma lista, em Python, é um conjunto de elementos. Tais elementos podem ser números, strings, objetos (como o quadradinho) e qualquer outra estrutura de Python, inclusive uma outra lista. Tais elementos podem também compor simultaneamente uma mesma lista, isto é, ela pode ser formada por elementos de tipos diferentes.

  Listas, em Python, são sequências de elementos que apresentam um número (também em sequência) para referenciar cada um deles. Esse número de referência chamamos de **índice**. Eles são implícitos, isto é, não aparecerem na estrutura, e sempre começam de 0 (a menos que você indique algo diferente).

  A figura abaixo mostra uma lista com 4 elementos. A cada elemento existe um índice associado, em sequência. O primeiro é 0, o segundo é 1, o terceiro é 2 e o quarto é 3.

  ![Listas](https://github.com/exxardo/assets/blob/main/ListasPython1.png)

  Uma lista serve também para representar um **vetor**, que é uma coleção de elementos do mesmo tipo, como por exemplo, só de números.

 Podemos indicar um nome/identificador a uma lista. Através deste nome, podemos referenciar a lista como um todo ou cada elemento dela separadamente.

```python
lista = []
lista = ['carro', 123, 123.0, True]
print(lista)
print(lista[3])

for item in lista:
    print(item)
    
for item in range(0, len(lista)): # De 0 até o tamanho da lista
    print(f'Indice: {item}, tem o elemento {lista[item]}') 
```

#### **Inserir e remover elementos de uma lista**

```python
lista = []
lista = ['carro', 123, 123.0, True]
print(lista)
print(lista[3])

for item in lista:
    print(item)
    
for item in range(0, len(lista)): # De 0 até o tamanho da lista
    print(f'Indice: {item}, tem o elemento {lista[item]}')
    
lista.append(246)

print(lista)
```

Em uma lista existe uma série de métodos que podem executar uma série de operações, isso tornam as listas muito ricas em funções (mas consomem mais recursos computacionais).

```python
ista = []
lista = ['carro', 123, 123.0, True]
print(lista)
print(lista[3])

for item in lista:
    print(item)
    
for item in range(0, len(lista)): # De 0 até o tamanho da lista
    print(f'Indice: {item}, tem o elemento {lista[item]}')
    
lista.append(123)

print(lista)

lista.remove(123)

print(lista)
```

Ele remove o primeiro que encontra.

#### **Count**

O count conta quantas vezes se repete cada um dos elementos.

#### **Reverse**

Inverte a ordem dos termos.

#### **Sort**

Ordena por ordem crescente itens de um mesmo tipo.

-----------------

### **Segunda-feira, 09/08/2021**

Estrutura padrão para criação de janelas no pygame:

```python
import pygame

pygame.init()

largura_tela = 800 # em pixels
altura_tela = 600 # em pixels

tela = pygame.display.set_mode(largura_tela, altura_tela)

pygame.quit()
```

Se o código for executado como está, não será possível finalizar a janela. É preciso criar um meio de saída.

```python
# Checar os eventos do mouse:
    for event in pygame.event.get():
        if event.type == pygame.QUIT:
            terminou = True
# Finaliza a tela
pygame.display.quit()
```

*Eventos são todas as ações do usuário, como cliques na tela.*

Para o `event` que ocorrer em `pygame.event.get()` nós teremos a verificação da condição: se `event.type ` for igual ao `pygame.QUIT`, então a variável `terminou` será igual `True` e fechará a janela do programa.

*OBS: o objeto de checagem no `pygame.event.get()` deve sempre se chamar `event`*

Por recomendação, assim como entramos e saímos do pygame, devemos finalizar a tela também

```python
import pygame

pygame.init()

# em pixels
largura_tela = 800
altura_tela = 600

tela = pygame.display.set_mode(largura_tela, altura_tela)

terminou = False
while not terminou:
    # Atualizar o desenho na tela:
    pygame.display.update()

    # Checar os eventos do mouse:
    for evento in pygame.event.get():
        if event.type == pygame.QUIT:
            terminou = True

pygame.quit()
```

Desenhar os retângulos e definir as cores:

```python
# Desenhando os retangulos
    # Definição das cores:
amarelo = (255, 255, 0)
vermelho = (255, 0, 0)

pygame.draw.rect(tela, amarelo, (10, 10, 200, 100))
```

*O código de desenhar deve ser inserido após a criação da tela*

```python
# Configurando tamanho da tela:
tela = pygame.display.set_mode((largura_tela, altura_tela))

# Desenhando os retangulos
    # Definição das cores:
amarelo = (255, 255, 0)
vermelho = (255, 0, 0)

pygame.draw.rect(tela, amarelo, (10, 10, 200, 100), 1)
pygame.draw.rect(tela, vermelho, (112, 140, 200, 100))
```

OBS: `pygame.draw.rect(tela, cor, (posição x, posição y, comprimento do retangulo, altura do retângulo), espessura da linha)`

#### Tempo (`pygame.time`)

É importante definir como o jogo vai se comportar em relação ao tempo, caso contrário o jogo irá acabar tendo por taxa básica o de cada máquina. Terá como consequência o tempo passando diferente em casa máquina.

```python
import pygame
import pygame.time

#Iniciando o pygame:
pygame.init()

# Dimensões da tela em pixels:
largura_tela = 800
altura_tela = 600

# Configurando tamanho da tela:
tela = pygame.display.set_mode((largura_tela, altura_tela))

# Desenhando os retangulos
    # Definição das cores:
amarelo = (255, 255, 0)
vermelho = (255, 0, 0)

pygame.draw.rect(tela, amarelo, (10, 10, 200, 100), 1)
pygame.draw.rect(tela, vermelho, (112, 140, 200, 100))

clock = pygame.time.Clock()

terminou = False
while not terminou:
    # Atualizar o desenho na tela:
    pygame.display.update()

    # Configura 60 atualizações de tela por segundo:
    clock.tick(60)

    # Checar os eventos do mouse:
    for event in pygame.event.get():
        if event.type == pygame.QUIT:
            terminou = True

# Finaliza a tela:
pygame.display.quit()

# Finalizando o pygame:
pygame.quit()
```
-------
### **Quarta-feira, 11/08/2021**

#### Como desenhamos vários quadradinhos diferentes sem repetir código?

Precisamos criar uma estratégia para fazer com que os valores se alterem a cada vez que o quadrado for desenhado.

#### Inserindo Quadradinhos Aleatoriamente

Para inserir quadradinhos em posições aleatórias e com cores aleatórias, você deve importar o módulo **random**.

```python
cor = (random.randint(20, 255), random.randint(20, 255), random.randint(20, 255))
```

Cada `random` é responsável por gerar uma cor aleatória em cada espectro: R, G, B.

```python
area = (random.randint(0, largura_tela - 30), random.randint(0, altura_tela - 30), 30, 30)
```

Cada `random` é responsável por gerar uma dimensão de lado aleatória. O tamanho do nosso quadrado irá de `0` até `largura_tela/altura_tela - 30`. A subtração se deve ao fato dos quadradinho já possuírem `30, 30` de altura e largura.

Caso não fosse diminuído esses valores, os quadradinhos apareceriam fora da tela.

#### Gerando vários qaudrados

Para isso iremos utilizar a repetição `for`:

```python
for quadrado in range(20):
# Cores e tamanhos aleleatórios
    cor = (random.randint(20, 255), random.randint(20, 255), random.randint(20, 255))
    area = (random.randint(0, largura_tela - 30), random.randint(0, altura_tela - 30), 30, 30)
    pygame.draw.rect(tela, cor, area)
```

O código funciona muito bem, mas podemos melhorar.

#### `class`

**Classe** é um artifício para **criar uma estrutura com todas as características e funções** necessárias para se trabalhar com um objeto (no caso, o quadradinho). Como vamos criar muitos quadradinhos, isto torna a programação mais simples e fácil de entender.

Para se criar uma classe é necessário desenvolver um método chamado ***método construtor***, onde eu passo as característica do objeto que quero construir.

```python
class desenhaQuadradinho():
    def __init__(self):
        self.largura = 30
        self.altura = 30
        self.x = random.randint(0, largura_tela - 30)
        self.y = random.randint(0, altura_tela - 30)
```

O **self** é usado em **classes** no **Python** para indicar que você está referenciando alguma coisa do próprio objeto (sejam eles atributos ou métodos) - na verdade, o **self** é o próprio objeto em si.

Nos métodos da classe você sempre passa o `self` (não é obrigatório) para que os métodos e atributos que você já definiu sejam acessíveis dentro do método que você está implementando. Em geral, quando criamos um construtor de uma classe (com o método `__init__`) colocamos o self nos atributos definidos ali para que eles sejam acessíveis nos métodos (raramente um atributo que você inicializa no construtor não será usado em algum canto na classe).



- ##### `__*__`

  Esses são nomes começados e terminados com dois underscores. Eles são conhecidos como *"métodos dunder"*. *"Dunder"* significa *"double underscore".*

  Exemplos: __init__ (lê-se *"dunder init"*), __next__ (*"dunder next"*), __getattr__, __del__, __str__, etc. Veja todos eles na seção [*Special method names*](http://docs.python.org/3/reference/datamodel.html#specialnames).

  Qualquer identificador nomeado dessa forma, é considerado reservado para a linguagem Python.

  Portanto, não crie variáveis, nem funções, nem métodos, nem classes, nem nada que o nome comece e termine com dois underscores. O manual avisa que isso pode causar problema de compatibilidade com a linguagem, sem nenhum aviso.

- ##### `_*`

  Nomes começados com um underscore, quando criados dentro de um módulo (ou pacote), não são importados quando usamos o comando `from modulo import *`.

  Se você está criando um módulo e quer que alguma função, variável ou classe não seja importada automaticamente, nomeie-a iniciando com um underscore. Exemplo: `_minha_funcao_interna()`.

  Usando o mesmo conceito de "interno", métodos e variáveis internas de uma classe que não devem ser acessados de fora dela, também devem começar com um underscore. Mas isso não os torna privados. Nesse caso, é apenas uma convenção mesmo.

  

- ##### `__*`

  Nomes começados com dois underscores são fonte de erro de interpretação e foram eles que me motivaram a escrever esse post. Eles são nomes de métodos ou variáveis privados de uma classe.

  Quando variáveis ou métodos com nomes assim são definidos dentro de uma classe, eles são modificados e têm o nome da classe inserido no início.

  [Um ou dois sublinhados? Mais informações](https://aprenda-python.blogspot.com/2013/05/um-ou-dois-sublinhados.html)

```python
class quadradinho():
    def __init__(self):
        self.largura = 30
        self.altura = 30
        self.x = random.randint(0, largura_tela - 30)
        self.y = random.randint(0, altura_tela - 30)
        self.area = pygame.Rect(self.x, self.y, self.largura, self.altura)
        self.cor = random.randint(20, 255), random.randint(20, 255), random.randint(20, 255) # R, G, B
        
    def desenhar(self, tela):
        pygame.draw.rect(tela, self.cor, self.area)
```

Para chamar a classe fazemos como se fosse uma função:

```python
for desenhar in range(0, quadrados_iniciais):        
    classQuadradinho = quadradinho()
    classQuadradinho.desenhar(tela)
```
----
### **Segunda-feira, 16/08/2021**

#### Capturando cliques do mouse

```python
        # Checar evento de clique no quadradinho:
        if event.type == pygame.MOUSEBUTTONDOWN and event.button == 1:
            posicao = pygame.mouse.get_pos()
            for classQuadradinho in lista:
                if classQuadradinho.area.collidepoint(posicao):
                    lista.remove(classQuadradinho)
```

Se `event.type ` for igual a `pygame.MOUSEBUTTONDOWN` e também a `event.button == 1`, então realizará a tarefa  `pygame.mouse.get_pos()`, que irá receber a posição do mouse e guardar na variável `pos`

- `event.type`: tipo de interação do usuário.
- `pygame.MOUSEBUTTONDOWN`: detecta de a tecla do mouse foi clicada.
- `event.button == 1`: a refere-se a qual botão foi clicado: 1 será o esquerdo e 2 o direito.
- `pygame.mouse.get_pos()`: recebe a posição do mouse

#### Atualizando tela

```python
    tela.fill(preto)
    for quadradinho in lista:
        quadradinho.desenhar(tela)
```
#### Código

```python
import pygame
import pygame.time
import random

#Iniciando o pygame:
pygame.init()

# Quantidade de quadrados apresentados
quadrados_iniciais = 20

largura_tela = 800 # Em pixels
altura_tela = 600 # Em pixels
preto = (0, 0, 0)
branco = (255, 255, 255)
terminou = False
tempo_inicial = 30 # Segundos
conta_clocks = 0
pontos = 0
conta_segundos = tempo_inicial

# Configurando tamanho da tela:
tela = pygame.display.set_mode((largura_tela, altura_tela))

def mostrar_tempo(tempo, pontos):
    font = pygame.font.Font(None, 24)
    text = font.render(f'Tempo: {tempo}s | Pontuação: {pontos}', 1, branco)
    textpos = text.get_rect(centerx=tela.get_width() / 2)
    tela.blit(text, textpos)
    
def mostrar_pontuacao_final(tela, pontos):
    tela.fill(preto) # Limpar toda a tela
    font = pygame.font.Font(None, 40)
    text = font.render(f'Pontuaçãoo: {pontos}s | Pontuação: {pontos}', 1, branco)
    textpos = text.get_rect(center=(tela.get_width() / 2, tela.get_height() / 2))
                            
clock = pygame.time.Clock()

class quadradinho():
    def __init__(self):
        self.largura = 30
        self.altura = 30
        self.x = random.randint(0, largura_tela - 30)
        self.y = random.randint(0, altura_tela - 30)
        self.area = pygame.Rect(self.x, self.y, self.largura, self.altura)
        self.cor = random.randint(20, 255), random.randint(20, 255), random.randint(20, 255) # R, G, B
        
    def desenhar(self, tela):
        pygame.draw.rect(tela, self.cor, self.area)

lista = []
for desenhar in range(0, quadrados_iniciais):        
    quadrado = quadradinho()
    quadrado.desenhar(tela)
    lista.append(quadrado)

while not terminou:
    # Atualizar o desenho na tela:
    pygame.display.update()

    # Configura 60 atualizações de tela por segundo:
    clock.tick(60)

    # Checar evento de saída:
    for event in pygame.event.get():
        if event.type == pygame.QUIT:
            terminou = True
    
        # Checar evento de clique no quadradinho:
        if event.type == pygame.MOUSEBUTTONDOWN and event.button == 1:
            posicao = pygame.mouse.get_pos()
            for quadrado in lista:
                if quadrado.area.collidepoint(posicao):
                    lista.remove(quadrado)
                    pontos = pontos + 1

    conta_clocks = conta_clocks + 1
    
    if conta_clocks == 60:
        if conta_segundos >= 0:
            conta_segundos = conta_segundos - 1
        conta_clocks = 0
        quadrado = quadradinho()
        lista.append(quadrado)

    if conta_segundos >= 0:
        tela.fill(preto)
        for quadrado in lista:
            quadrado.desenhar(tela)
            mostrar_tempo(conta_segundos, pontos)

    else:
        mostrar_pontuacao_final(tela, pontos)
        for quadrado in lista:
            lista.remove(quadrado)
# Finaliza a tela:
pygame.display.quit()

# Finalizando o pygame:
pygame.quit()
```



#### Relembrando:

Orientação a objetos vem para organizar a forma de se programar.

- Programação estruturada:
  - C, Pascal, Fortran, Python, etc.
- Programa é definido através de uma sequência de instruções e chamadas de funções que manipulam os dados;
- Ótima opção para códigos pequenos e de rápida implementação.
- As características são chamadas de atributos, mas quando falamos de Orientação a Objetos usamos outro termo: **estado**.
  - Os atributos mudam o estado dos objetos.

Programação Orientada a Objetos

- Diferente da programação estruturada;
- Modelo de programação que reflete melhor o mundo real;
- Mais fácil de compreender e modelar o problema no código.

![Exemplo](https://github.com/exxardo/assets/blob/main/Captura%20de%20tela%202021-08-25%20224739.png)

- Classe é um molde de objetos;
- Possui as informações/variáveis (atributos) e as funções (métodos) que os objetos vão poder exercer;
- Objetos são gerados a partir das classes;
- Essa “criação” é chamada de instaciação do objeto;
- E aquele objeto passa a ser uma instância da classe.

> - Instancia é um objeto personalizado que acabou de ser fabricado baseando-se nas especificações de uma classe. O objeto pode, agora, ser utilizado para executar funções ou guardar dados.

![Exemplo](https://github.com/exxardo/assets/blob/main/Captura%20de%20tela%202021-08-25%20225655.png)

![Exemplo](https://github.com/exxardo/assets/blob/main/Captura%20de%20tela%202021-08-25%20225805.png)

- O grupo de objetos criados forma então a sua aplicação/programa.

##### A porta

- A porta, por exemplo, é um objeto da classe Porta;
- Possui um **atributo** cor cujo **valor** é “amarelo”;
- Possui dois métodos **(ações)**: abrir e fechar.

##### Carro

- Atributos:
  - Cor;
  - Marca;
  - Modelo.
- Métodos:
  - ligarMotor()
  - desligarMotor()
  - andar()
  - parar()

![Exemplo](https://github.com/exxardo/assets/blob/main/Captura%20de%20tela%202021-08-25%20231024.png)

```python
class Carro:
    cor = 'sem cor'
    marca = 'sem marca'
    modelo = 'sem modelo'
    ano = 2010
    km_rodados = 0

def detalhes(self):
    print ('cor:', self.cor)
    print ('marca:', self.marca)
    print ('modelo:', self.modelo)
    print ('ano:', self.ano)
    print ('km rodados:', self.km_rodados)
```

```python
# Console

>>>car_1 = Carro() #Instância o objeto da classe Carro na variável 'car_1'
>>>car_1.cor = 'Vermelho'
>>>car_1.marca = 'Honda'
>>>car_1.modelo = 'HR-V'
>>>car_1.ano = 2016
>>>car_1.detalhes() #Chama o método 'detalhes' implementado na classe Carro

cor: Vermelho
marca: Honda
modelo: HR-V
ano: 2016
km_rodados: 0
```

------

### **Quarta-feira, 18/08/2021**

#### Strings são imutáveis

É tentador usar o operador [] no lado esquerdo de uma atribuição, com a intenção de alterar um caractere em uma string. Por exemplo:

```python
greeting = 'Hello, world!'
greeting[0] = 'J'

TypeError: 'str' object does not support item assignment
    
# O “objeto” neste caso é a string e o “item” é o caractere que você tentou atribuir. Por enquanto, um objeto é a mesma coisa que um valor, mas refinaremos esta definição mais adiante (Objetos e valores, no capítulo 10).
```

A razão do erro é que as strings são imutáveis, o que significa que você não pode alterar uma string existente. O melhor que você pode fazer é criar uma string que seja uma variação da original:

```python
greeting = 'Hello, world!'
new_greeting = 'J' + greeting[1:]
new_greeting

'Jello, world!'

# Esse exemplo concatena uma nova primeira letra a uma fatia de greeting. Não tem efeito sobre a string original.
```

[Pense em Python](https://pense-python.caravela.club/)

#### Print de varíaveis longas

```python
teste = '''
              BEM-VINDO
        AO MEU TESTE DE PRINT'''

print(teste)
```

[Tutoriais e Exercícios](https://www.w3resource.com/)

------

### **Segunda-feira, 23/08/2021**

#### Dicionários

[Python Dictionaries](https://www.w3schools.com/python/python_dictionaries.asp)

Dicionários tem a tarefa de criar o que chamamos de **mapeamento**.

Exemplo:

```python
thisdict = {
    'brand': 'Ford',
    'model': 'Mustang',
    'year': 1964
}
```

Quando vamos trabalhar com dicionários não temos índice, com em listas/tuplas, será pelo o que chamamos de **key**.

![KeyValue](https://github.com/exxardo/assets/blob/main/williams_kv_db_1.png)

**Todas as chaves precisam ser strings?**

Não, mas definida uma chave ela não poderá ser mudada. O que faz sentido é alterar o valor da chave.

#### Agenda

Como organizar os nomes e telefones dos seus amigos em Python?

##### Opção 1: usar uma lista

- Lista contém nome seguido de um ou mais telefones

  ```python
  listaNomeTels = ["Maria", [99887766, 99887755], "Pedro", [92345678], "Joaquim", [99887711, 99665533]]
  ```

- Como recuperar o telefone de Maria?

  ```python
  tel = listaNomeTels[listaNomeTels.index("Maria") + 1]
  >>> tel
  
  # Retorno
  [9988776, 99887755]
  ```

  Muito complicado

###### Alterações?

| **Remover contato**      | **Exige remover dois elementos da lista...**                 |
| ------------------------ | ------------------------------------------------------------ |
| **Remover Telefone**     | **Exige remover  um elemento de uma lista que está armazenada dentro de outra...** |
| **Acrescentar Telefone** | **Exige acrescentar um elemento em uma lista que está armazenada dentro de outra...** |

##### Opção 2: usar duas listas

- Uma listas com os nomes

- Uma segunda lista com os telefones

- Correspondência pelas posições

  ```python
  listaNomes = ["Maria", "Pedro", "Joaquim",]
  listaTelefones = [[99887766, 99887755], [92345678], [99887711, 99665533]]
  ```

###### Como recuperar o telefone de Maria?

```python
tel = listaTelefones[listsaNomes.index("Maria")]
>>> tel

# Retorno
[99887766, 99887755]
```

Duas listas me faria ter que fazer ligações de um nome em uma lista com o telefone em outras. Teria também que fazer várias operações para remover e alterar.

##### Resumindo:

- Usando listas, a única forma de **indexação é usando números inteiros** (posição na lista)
- Isso sempre exige uma busca na lista auxiliar para encontrar a posição desejada a ser usada para recuperar a informação desejada na lista.

##### Alternativa: Dicionário

- Estrutura de dados que implementa mapeamentos entre uma chave (**key**) e algum conteúdo (**value**)
  - Mapeamentos também são chamados de pares chave-valor
- A chave funciona como um índice para acessar o condeúdo
- Conteúdo pode ser qualquer coisa, inclusive outro dicionário

![Dict](https://github.com/exxardo/assets/blob/main/Captura%20de%20tela%202021-08-29%20112405.png)

##### Voltando ao exemplo da agenda

- Qual dado deve servir como chave?
  - Por qual elemento quero fazer o acesso?

No nosso caso precisamos definir então: **nome**

- Qual dado deve servir como conteúdo?
  - Qual(is) valor(es) quero associar à chave?

Nesse caso: **telefone**

Dicionário onde a chave é o **nome** e o conteúdo é a **lista de telefones**

![Dict](https://github.com/exxardo/assets/blob/main/Captura%20de%20tela%202021-08-29%20114318.png)

```python
agenda = {
    "Maria": [99887766, 99887755],
    "Pedro": [92345678],
    "Joaquim": [99887711, 99665533]
}
```

Alterando:

```python
agenda = {
    "Maria": [99887766, 99887755],
    "Pedro": [92345678],
    "Joaquim": [99887711, 99665533]
}

agenda["Pedro"] = 98480296
```

Funcionaria? Sim, mas não é uma forma muito elegante de se fazer.

##### Ordem

- As chaves dos dicionários não são armazenadas em nenhuma ordem específica
  - Na verdade, dicionários são implementados por tabelas de espelhamento (**Hash Tables**)
  - A falta de ordem é proposital

##### Dicionários x Listas

- Diferentemente de listas, atribuir a um elemento de um dicionário não requer que a posição exista previamente (isso ocorre porque não se trata de posição, e sim de valor de chave!)

```python
lista = []
lista[10] = 5 # ERRO!

dicionario = {}
dicionario[10] = 5 # OK!
dicionario

# Retorno
{10: 5}
```

- Varável do tipo dicionários também armazena endereço de memória

  - Com dicionários, ocorre o mesmo efeito que ocorre com cópia de listas - o que é copiado é o endereço de memória, e portanto, alterações nas cópias são refletidas umas nas outras

  ```python
  d1 = {"Catarina": 5}
  d2 = d1
  d1["Jonas"] = 20
  
  print(d1)
  print(d2)
  ```

  Será as mesmas chaves e mesmos valores em ambos. 

#### `copy()`

Retorna um outro dicionário com os mesmo pares chave/valor

#### `clear()`

Remove todos os elementos do dicionário

#### `update()`

É a maneira correta de se adicionar novos elementos em um dicionário.

```python
dic = {'Eduardo': 'Lenovo'}
dic.update({'Lucas': 'Dell'})
```

#### Usando Python Preview no VS Code

```python
dic = {
    'Vermelho': 1,
    'Verde': 2
    'Azul': 3
}

for key, value in dic.items():
    print(f'{key} corresponde a {dic[key]})
```

#### `del` (deletar)

```python
my_dict = {
    'a': 1, 
    'b': 2, 
    'c': 3, 
    'd': 4
}
if 'b' in my_dict:
    del my_dict['b']
```

#### `sorted` (ordenar)

```python
color_dict = {
    'red': '#FF0000', 
    'green': '#008000',
    'black': '#000000',
    'white': '#FFFFFF'
}

for key in sorted(color_dict):
    print(f'A cor {key} corresponde ao código {color_dict[key]}')
```

O `sorted` ordenou as cores por ordem alfabética

OBS: o `color_dict[key]` me traz o valor da chave.

##### `sorted` em ordem decrescente

```python
color_dict = {
    'red': '#FF0000', 
    'green': '#008000',
    'black': '#000000',
    'white': '#FFFFFF'
}

for key in sorted(color_dict, reverse = True):
    print(f'A cor {key} corresponde ao código {color_dict[key]}')
```

#### Unir dicionários

```python
dic1 = {
    1: 10, 
    2: 20
}
dic2 = {
    3: 30, 
    4: 40
}
dic3 = {
    5: 50, 
    6: 60
}
dic4 = {}

for key in (dic1, dic2, dic3):
    dic4.update(item)
    print(dic4)
```

#### `len`

Conta quantos objetos há.

-----

### **Quarta-feira, 25/08/2021**

#### Ciência de Dados: a revolução na tomada de decisões

- Dados não correlacionados não são informações.

##### Roteiro

- Introdução à Data Science
- Projetos na Área de Dados
- Profissionais dos Dados
- Como se tornar um Profissional da Área
- Conclusão

##### A área nem existiu, mas vem mudando com o tempo

- (1970-1985) **Suporte à decisão**: Uso da análise de dados para dar suporte a decisões
- (1990-1990) **Suporte aos executivos**: Foco na análise de dados para dar suporte ao processo dei=cisório dos altos executivos
- (1990-2000) **Processamento analítico on-line - OLAP**: Software para análise de tabelas de dados multidimensionais
- (1989-2005) **Business Intelligence**: Ferramentas para dar suporte a decisões orientadas por dados, com ênfase em relatórios
- (2005-2010) **Analytics**: Foco em análise estatísticas e matemáticas para a tomada de decisões
- (2010-Atual) **Big Data Analytics**: Foco em um grande volume de dados não estruturados e em rápido movimento

##### Nubank é uma das empresas brasileiras mais orientadas a dados

- Concessão de Crédito
- Atualização de Limite
- Análise de Fraude e Risco
- ...

##### Healthcare - Análise preditiva em Saúde

- Predição de risco clínico 
- Saúde Pública

##### A área de dados é transversal e tem como objetivo gerar valor nas decisões

1. **Decisões**: Decisões em dados.
2. **Informação**: Geração de insights e previsões do problema.
3. **Dados**: Diferentes fontes de dados, interna e externas sobre o problema.

##### Qual o perfil do profissional que trabalha com dados?

![Data Scientist](https://github.com/exxardo/assets/blob/main/Captura%20de%20tela%202021-08-29%20193046.png)

##### Data Scientist - Negócio

- Conhecimento do Negócio
- Traduzir o Problema de Negócio para modelagem matemática

##### Data Scientist - Data Management

- Consultas em diferentes Bases - SQL
- Big Data
- Mineração de Dados

##### Data Scientist - Analytics

- Inferência **Estatística**
- Algoritmos de Machine Learning
- Análise de Dados

##### Data Scientist - Art & Design

- Visualização de Dados
- Apresentação de Resultados
- Storytelling

##### Data Scientist - Computer Science

- Programação
- Integração entre Sistemas
- Engenharia de Software

##### Diferentes profissionais podem trabalhar na área de dados

- Engenheiro de Dados
  - Coleta de dados
  - Infraestrutura de Armazenamento
- Cientista de Dados
  - Análise estatística
  - Modelagem
  - Visualização
- Engenheiro de Machine Learning
  - Operacionalizar modelos
  - Integração

##### Níveis de maturidade das empresas com relação ao uso dos Dados

1. **Empírico**: Ambiente caótico. Sem coleta de dados e decisões empíricas individualizadas.
2. **Adhoc**: A maioria das mepresas brasileiras. Dados coletados sem uma arquitetura de informação orientada a dados.
3. **Definido**: Dados coletados, com indicadores validados e orientados à cultura de dados. Decisões pautadas em sistemas de monitoramento e BI.
4. **Otimizado**: Dados coletados e enriquecidos. Geração automáticas de análises preditivas e prescritivas. Decisões baseadas em métricas.

##### Utilize as comunidades e plataforma de Data Science para trocar experiências

- Plataformas de competição de Data Science são uma ótima forma de aprender a modelar problemas de dados.
  - Kaggle
  - Data Harckers
  - Meetup

##### Explore os dados e conte uma história

- Utilize datasets públicos para fazer análises exploratórias e contar histórias sobre um determinado objetivo.
  - Kaggle
  - Google Dataset Search
  - Portal Brasileiro de Dados Abertos

 [Primeiros passos com Web Scraping](https://www.section.io/engineering-education/getting-started-with-web-scraping-using-python/)

----

### **Segunda-feira, 30/08/2021**

#### O que é Web Scraping?

Técnica de computação usada para extrair informações de websites. esta técnica geralmente foca na transformação de dados estruturados para a Web, em formato HTML, para alguma estrutura particular de interesse do programador.

#### Fazendo Web Scraping em CSV

#### `urllib3`

```python
import urllib3

url = 

http = urllib3.PoolManager()
response = http.request('GET', url) 
csv = response.data

csv
```

##### Métodos

O `GET` solicita os dados do endereço recebido pela variável.

`POTS` salva as informações lá

#### `requests`

```python
import requests

url = 

csv = request.get(url).text

csv
```

##### Separando por linhas

```python
import requests

url = 

csv = request.get(url).text

linhas = csv.splitlines()

linhas
```

##### Extrair valores de determinada linha

```python
import requests

url = 

csv = request.get(url).text

linhas = csv.splitlines()

for lin in linhas:
    colunas = lin.split(',')
    
colunas
```

Ele armazenou apenas a última linha na variável.

#### Exercício 1:

#### 
